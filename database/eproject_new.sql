-- Database name: eproject

-- create database with name 'eproject'
CREATE DATABASE `eproject`;

-- use database before create table
USE `eproject`;

-- create brand table
CREATE TABLE `brand`
(
    `id`         int          NOT NULL AUTO_INCREMENT,
    `name`       varchar(255) NOT NULL,
    `brand_logo` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create category table
CREATE TABLE `category`
(
    `id`     int          NOT NULL AUTO_INCREMENT,
    `name`   varchar(255) NOT NULL,
    `slug`   varchar(255) NOT NULL,
    `status` tinyint(1)   NOT NULL DEFAULT '1',
    `parent` int          NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create product table
CREATE TABLE `product`
(
    `id`          int          NOT NULL AUTO_INCREMENT,
    `name`        varchar(255) NOT NULL,
    `price`       int          NOT NULL,
    `sale_price`  int          NOT NULL,
    `intro`       text         NOT NULL,
    `content`     text         NOT NULL,
    `image`       varchar(255) NOT NULL,
    `status`      tinyint(1)   NOT NULL DEFAULT '1' COMMENT '0 not available - 1 in stock',
    `featured`    tinyint(1)   NOT NULL DEFAULT '0' COMMENT '0 featured',
    `category_id` int          NOT NULL,
    `brand_id`    int          NOT NULL,
    PRIMARY KEY (`id`),
    KEY `product_brand_id_fk` (`brand_id`),
    KEY `product_category_id_fk` (`category_id`),
    CONSTRAINT `product_brand_id_fk` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
    CONSTRAINT `product_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create user table
CREATE TABLE `user`
(
    `id`       varchar(300) NOT NULL COMMENT 'generate id by UUID',
    `email`    varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `level`    tinyint(1)   NOT NULL DEFAULT '2' COMMENT '2 customer - 1 admin',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create user_info table
CREATE TABLE `user_info`
(
    `id`        int          NOT NULL AUTO_INCREMENT,
    `full_name` varchar(255) NOT NULL,
    `address`   varchar(255) NOT NULL,
    `phone`     int          NOT NULL,
    `user_id`   varchar(300) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `user_info_user_id_fk` (`user_id`),
    CONSTRAINT `user_info_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create wish_list table
CREATE TABLE `wish_list`
(
    `id`         int          NOT NULL AUTO_INCREMENT,
    `user_id`    varchar(300) NOT NULL,
    `product_id` int          NOT NULL,
    PRIMARY KEY (`id`),
    KEY `wish_list_product_id_fk` (`product_id`),
    KEY `wish_list_user_id_fk` (`user_id`),
    CONSTRAINT `wish_list_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
    CONSTRAINT `wish_list_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create cart table
CREATE TABLE `cart`
(
    `id`         int          NOT NULL AUTO_INCREMENT,
    `created_at` datetime     NOT NULL,
    `user_id`    varchar(300) NOT NULL,
    PRIMARY KEY (`id`),
    KEY `cart_user_id_fk` (`user_id`),
    CONSTRAINT `cart_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

-- create cart_detail table
CREATE TABLE `cart_detail`
(
    `id`          int NOT NULL AUTO_INCREMENT,
    `price`       int NOT NULL,
    `quantity`    int NOT NULL,
    `total_price` int NOT NULL,
    `cart_id`     int NOT NULL,
    `product_id`  int NOT NULL,
    PRIMARY KEY (`id`),
    KEY `cart_detail_cart_id_fk` (`cart_id`),
    KEY `cart_detail_product_id_fk` (`product_id`),
    CONSTRAINT `cart_detail_cart_id_fk` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
    CONSTRAINT `cart_detail_product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
<?php
session_start();
ob_start();

include './core/config.php';
include './core/connect.php';
include './core/function.php';
include './model/user.php';
include './model/product/product.php';
include './model/slider.php';
include './model/banner.php';
include './model/brand.php';
include './model/category.php';

$errors = array();
$success = false;
$old_data = $_SESSION["customer_profile"];
$id = $old_data["id"];
$province = $old_data["province_city"];
$district = $old_data["district"];
$ward = $old_data["ward"];
$address_detail = $old_data["address_detail"];

if (!isset($old_data)) {
    header("location:index.php");
    exit();
}

if (isset($_POST["cancel"])) {
    header("location:index.php");
    exit();
}

if (isset($_POST["logout"])) {
    header("location:logout.php");
    exit();
}

if (isset($_POST["submit"])) {
    if (empty($_POST["full_name"])) {
        $errors[] = "Please enter your full name.";
    } else {
        $name = $_POST["full_name"];
        if (!preg_match("/^([a-zA-Z' ]+)$/", $name)) {
            $errors[] = "Full name does not contain number or special characters.";
        }
    }

    if (empty($_POST["phone"])) {
        $errors[] = "Please enter phone number.";
    } else {
        if (!is_numeric($_POST["phone"])) {
            $errors[] = "Phone number must be a number.";
        }
        if (strlen($_POST["phone"]) > 11) {
            $errors[] = "Phone number must be between 10 and 11 characters.";
        }
    }

    if (!empty($_POST["province_city"])) {
        if (empty($_POST["district"])) {
            $errors[] = "Please select a district.";
        }
        if (empty($_POST["ward"])) {
            $errors[] = "Please select a ward.";
        }
        if (empty($_POST["address_detail"])) {
            $errors[] = "Please address details (Apartment No., Street).";
        }
    }

    if (!empty($_POST["password"])) {
        $password = $_POST["password"];
        if (strlen($password) < 8) {
            $errors[] = "Password must be at least 8 characters.";
        }
        if (!preg_match("/\d/", $password)) {
            $errors[] = "Password must be contain at least one number.";
        }
        if (!preg_match("/[A-Z]/", $password)) {
            $errors[] = "Password muse be contain at least one uppercase letter.";
        }
        if (!preg_match("/[a-z]/", $password)) {
            $errors[] = "Password must be contain at least one lowercase letter.";
        }
        if (!preg_match("/\W/", $password)) {
            $errors[] = "Password must be contain at least one special character.";
        }
        if (preg_match("/\s/", $password)) {
            $errors[] = "Password should not contain any white space.";
        }
        if (empty($_POST["confirm_password"])) {
            $errors[] = "Please enter a confirm password.";
        } else {
            if ($_POST['password'] !== $_POST['confirm_password']) {
                $errors[] = "Password does not match.";
            }
        }
    }

    if (empty($errors)) {
        $data = array(
            "status" => (isset($_POST["status"]) && ($_POST["status"] == 'on')) ? 1 : 0,
            "full_name" => $_POST["full_name"],
            "phone" => $_POST["phone"],
            "address_detail" => $_POST["address_detail"],
            "user_id" => $id
        );

        if (isset($_POST["password"])) {
            $data["password"] = $_POST["password"];
        }

        if (isset($_POST["province_city"])) {
            $data["province_city"] = $_POST["province_city"];
            $data["district"] = $_POST["district"];
            $data["ward"] = $_POST["ward"];
        }

        if (!empty($_FILES['avatar_img']["name"])) {
            $imageFileName = change_name_file($_FILES['avatar_img']["name"]);
            if (!check_ext($_FILES['avatar_img']["type"])) {
                $errors[] = "Only accept JPEG, JPG, PNG, GIF";
            } else {
                $data["avatar_img"] = $imageFileName;
            }
        } else {
            $data["avatar_img"] = $old_data["avatar_img"];
        }

        update_user($conn, $data, $success);

        if ($success) {
            if (!empty($_FILES['avatar_img']["name"])) {
                move_uploaded_file($_FILES['avatar_img']["tmp_name"], './ap-admin/public/assets/user_image/' .
                    $data["avatar_img"]);
            }

            $response = get_user_info($conn, $id);

            if ($response) {
                $_SESSION["customer_profile"] = $response[0];
                header("location:index.php");
                exit();
            }
            header("location:index.php");
            exit();
        }
    }
}
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php include './blocks/head.php' ?>
</head>

<body>

<!--header area start-->

<!--offcanvas menu area start-->
<?php include './blocks/canvas-menu.php' ?>
<!--offcanvas menu area end-->

<header>
    <?php include './blocks/header.php' ?>
</header>
<!--header area end-->

<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb_content">
          <h3>My Account</h3>
          <ul>
            <li><a href="index.php">home</a></li>
            <li>My account</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--breadcrumbs area end-->


<!-- my account start  -->
<section class="main_content_area">
  <div class="container">
    <div class="account_dashboard">
      <div class="row">
        <div class="col-sm-12 col-md-3 col-lg-3">
          <!-- Nav tabs -->
          <div class="dashboard_tab_button">
            <ul role="tablist" class="nav flex-column dashboard-list" id="nav-tab">
              <li><a href="#account-details" data-toggle="tab" class="nav-link active">Account details</a></li>
              <li><a href="#logout" data-toggle="tab" class="nav-link">log out</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-12 col-md-9 col-lg-9">
          <!-- Tab panes -->
          <div class="tab-content dashboard_content">
            <div class="tab-pane fade show active" id="account-details">
              <h3>Account details </h3>
              <div class="login">
                <div class="login_form_container">
                  <div class="account_login_form">
                    <form action="" method="POST"
                          enctype="multipart/form-data">
                      <br>
                      <!--                      customer avatar-->
                      <div class="customer-avatar">
                        <div class="customer-avatar-item">
                          <img
                              src="<?php echo "./ap-admin/public/assets/user_image/" . $_SESSION["customer_profile"]["avatar_img"] ?>"
                              alt="<?php echo "customer-" . rand() ?>"
                              onerror="this.src='./public/assets/image/no-image.png'"
                          />
                        </div>
                      </div>
                      <!--                      /customer avatar-->
                      <!-- Alert start -->
                        <?php if (!empty($errors)) { ?>
                          <div class="alert alert-danger alert-dismissible">
                            <h5><span class="lnr lnr-cross-circle"></span> Alert!</h5>
                              <?php
                              foreach ($errors as $error) {
                                  echo '<li>' . $error . '</li>';
                              }
                              ?>
                          </div>
                        <?php } ?>
                      <!-- /.alert -->

                      <!--                      full name-->
                      <label>Full Name</label>
                      <input type="text" name="full_name"
                             placeholder="Please enter your full name" <?php keep_value_input('full_name', $old_data["full_name"]);
                      ?>>

                      <!--                      email-->
                      <label>Email</label>
                      <input type="text" name="email" value="<?php echo $old_data["email"] ?>" disabled>

                      <!--                      phone-->
                      <label>Phone</label>
                      <input type="text" name="phone"
                             placeholder="Please enter phone number" <?php keep_value_input('phone', $old_data["phone"]); ?>>

                      <!--                      password-->
                      <label>New Password</label>
                      <input type="password" name="password" placeholder="Please enter your password">

                      <!--                      confirm password-->
                      <label>Confirm New Password</label>
                      <input type="password" name="confirm_password" placeholder="Please enter confirm password">

                      <!--                      address-->
                      <div class="address-title">
                        <label>Address</label>
                          <?php if ($address_detail && $ward && $district && $province) { ?>
                            <label class="edit-address-btn"
                                   onclick="onBtnExpandEditAddress()"
                            >Edit Address Here</label>
                          <?php } ?>
                      </div>
                        <?php if ($address_detail && $ward && $district && $province) { ?>
                          <input type="text" placeholder="Please enter user address details." disabled
                                 value="<?php display_address($conn, $address_detail, $ward, $district, $province);
                                 ?>" />
                        <?php } ?>
                      <div id="edit-address-detail"
                           class="edit-address-detail <?php if (!$address_detail && !$ward && !$district &&
                               !$province) {
                               echo 'edit-address-active';
                           } ?>">
                        <div class="row select-customize"
                             style="margin-bottom: 20px">
                          <div class="col-6 category-hover">
                            <select class="address-dropdown"
                                    name="province_city" id="province"
                                    onchange="addressCodeSelect('province')">
                              <option value="" selected disabled>Select a province/city</option>
                                <?php
                                if (isset($_POST["province_city"])) {
                                    keep_province_address_option($conn, $_POST["province_city"]);
                                } else {
                                    keep_province_address_option($conn, $province);
                                }
                                ?>
                            </select>
                          </div>
                          <div class="col-6">
                            <select class="address-dropdown" name="district" id="district" onchange="addressCodeSelect
                                                ('district')">
                              <option value="" selected disabled>Select a district</option>
                                <?php
                                if (isset($_POST["district"]) && isset($_POST["province_city"])) {
                                    keep_district_address_option($conn, $_POST["province_city"], $_POST["district"]);
                                } else {
                                    keep_district_address_option($conn, $province, $district);
                                }
                                ?>
                            </select>
                          </div>
                        </div>
                        <div class="row select-customize">
                          <div class="col-6">
                            <select class="address-dropdown" id="ward" name="ward">
                              <option value="" selected disabled>Select a ward</option>
                                <?php
                                if (isset($_POST["district"]) && isset($_POST["province_city"]) && isset($_POST["ward"])) {
                                    keep_ward_address_option($conn, $_POST["district"], $_POST["ward"]);
                                } else {
                                    keep_ward_address_option($conn, $district, $ward);
                                }
                                ?>
                            </select>
                          </div>
                          <div class="col-6">
                            <input type="text" name="address_detail"
                                   placeholder="House number and street name" <?php keep_value_input('address_detail', $address_detail);
                            ?>>
                          </div>
                        </div>
                      </div>

                      <!--                      avatar upload-->
                      <label>Update avatar</label>
                      <img src="" alt="">
                      <input type="file" name="avatar_img"
                             class="upload-img">

                      <!--                      submit-->
                      <div>
                        <button type="submit" name="submit">Save
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="logout">
              <div class="logout-form">
                <form action="" method="POST">
                  <h3>log out</h3>
                  <p>Are you sure you want to logout?</p>
                  <div class="logout-btn">
                    <button class="btn-logout-true" type="submit" name="logout" style="margin-left:0">Yes</button>
                    <button type="submit"
                            name="cancel"
                            style="background: unset; color: #40A944"
                    >Cancel
                    </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- my account end   -->

<!--footer area start-->
<footer class="footer_widgets footer_border">
    <?php include './blocks/footer.php' ?>
</footer>
<!--footer area end-->

<!--foot-->
<?php include './blocks/foot.php' ?>

</body>

</html>
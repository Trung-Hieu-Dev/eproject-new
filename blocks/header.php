<?php
$all_brands = list_brand($conn);
?>

<div class="main_header">

  <div class="header_middle">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-2 col-md-3 col-sm-3 col-3">
          <div class="logo">
            <a href="index.php"><img src="./public/assets/img/logo/logo.png" alt=""></a>
          </div>
        </div>
        <div class="col-lg-10 col-md-6 col-sm-7 col-8">
          <div class="header_right_info">
            <div class="search_container mobail_s_none">
              <!--              <div class="form-search">-->
              <form onsubmit="return false">
                <div class="hover_category select-brand">
                  <select class="select_option" name="select_brand" id="categori2" onchange="onSelectBrand(this)">
                    <option selected value="">Select a brand</option>
                      <?php foreach ($all_brands as $item) { ?>
                        <option value="<?php echo $item["name"] ?>"><?php echo $item["name"] ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="search_box">
                  <input class="search-product" name="search_name" placeholder="Search product..." type="text"
                         autocomplete="off">
                  <button id="search-btn" onclick="onSearchBtnClick(0)"><span class="lnr
                  lnr-magnifier"></span></button>
                  <div class="search-result" class="search-result">
                    <ul id="result" class="result"></ul>
                  </div>
                </div>
              </form>
              <!--              </div>-->
            </div>
            <div class="header_account_area">
              <!-- <div class="header_account_list register">
                <ul style="display:flex;justify-content: center;align-items: center;">
                    <?php if (!isset($_SESSION["customer_profile"])) { ?>
                      <li><a href="register.php">Register</a></li>
                      <li><span>/</span></li>
                      <li><a href="login.php">Login</a></li>
                    <?php } else { ?>
                      <li class="customer-detail">
                        <a href="account.php" class="d-flex justify-content-center align-items-center">
                          <div class="customer-avatar">
                            <img
                                src="<?php echo "./ap-admin/public/assets/user_image/" . $_SESSION["customer_profile"]["avatar_img"] ?>"
                                alt="<?php echo "customer-" . rand() ?>"
                                onerror="this.src='./public/assets/image/no-image.png'" />
                          </div>
                            <?php echo $_SESSION["customer_profile"]["full_name"] ?>
                        </a>
                      </li>
                    <?php } ?>
                </ul>
              </div> -->
              <div class="d-flex header_account_list header_wishlist">
                  <?php
                  $total_quantity = 0;
                  if (isset($_SESSION['wishlist']) && (!empty($_SESSION['wishlist']))) {
                      foreach ($_SESSION['wishlist'] as $id => $quantity) {
                          $total_quantity += $quantity;
                      }
                  }
                  ?>
                <a href="index.php?p=wishlist"><span class="lnr lnr-heart"></span> <span
                      class="item_count"><?php echo($total_quantity) ?></span> </a>
              </div>
              <div class="d-flex header_account_list mini_cart_wrapper">
                  <?php
                  $total_quantity = 0;
                  if (isset($_SESSION['cart']) && (!empty($_SESSION['cart']))) {
                      foreach ($_SESSION['cart'] as $id => $quantity) {
                          $total_quantity += $quantity;
                      }
                  }
                  ?>
                <a href="javascript:void(0)"><span class="lnr lnr-cart"></span><span
                      class="item_count"><?php echo $total_quantity ?></span></a>

                <!--mini cart-->
                <div class="mini_cart">
                  <div class="cart_gallery">
                    <div class="cart_close">
                      <div class="cart_text">
                        <h3>cart</h3>
                      </div>
                      <div class="mini_cart_close">
                        <a href="javascript:void(0)"><i class="icon-x"></i></a>
                      </div>
                    </div>
                      <?php
                      $total = 0;
                      if (isset($_SESSION['cart'])) {
                          foreach ($_SESSION['cart'] as $id => $quantity) {


                              $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
                              $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                              $stmt->execute();
                              $product = $stmt->fetch(PDO::FETCH_ASSOC);

                              ?>
                            <div class="cart_item">
                              <div class="cart_img">
                                <a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><img
                                      src="<?php echo $product['image'] ?>"></a>
                              </div>
                              <div class="cart_info">
                                <a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><?php echo $product['name'] ?></a>
                                <p><?php echo $quantity ?> x <span> USD <?php
                                        $total += $product['sale_price'] * $quantity;
                                        echo number_format($product['sale_price'] * $quantity, "0", "", ".")
                                        ?> </span></p>
                              </div>
                              <div class="cart_remove">
                                <a href="#" class="removeItemCart" data-id="<?php echo $id ?>"><i
                                      class="icon-x"></i></a>
                              </div>
                            </div>
                          <?php }
                      } ?>

                  </div>
                  <div class="mini_cart_table">
                    <div class="cart_table_border">
                      <!-- <div class="cart_total">
                        <span>Sub total:</span>
                        <span class="price">$125.00</span>
                      </div> -->
                      <div class="cart_total mt-10">
                        <span>total:</span>
                        <span class="price">USD <?php echo number_format($total, "0", "", ".") ?></span>
                      </div>
                    </div>
                  </div>
                  <div class="mini_cart_footer">
                    <div class="cart_button">
                      <a href="index.php?p=cart"><i class="fa fa-shopping-cart"></i> View cart</a>
                    </div>
                    <div class="cart_button">
                      <a href="index.php?p=checkout"><i class="fa fa-sign-in"></i> Checkout</a>
                    </div>

                  </div>
                </div>
                <!--mini cart end-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="header_bottom sticky-header">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-6 mobail_s_block">
          <div class="search_container">
            <!--            <div class="form-search">-->
            <form onsubmit="return false">
              <div class="hover_category">
                <select class="select_option" name="select_brand" id="categori2" onchange="onSelectBrand(this)">
                  <option selected value="">Select a brand</option>
                    <?php foreach ($all_brands as $item) { ?>
                      <option value="<?php echo $item["name"] ?>"><?php echo $item["name"] ?></option>
                    <?php } ?>
                </select>
              </div>
              <div class="search_box">
                <input class="search-product" name="search_name" placeholder="Search product..." type="text"
                       autocomplete="off">
                <button name="search_btn_mobile" onclick="onSearchBtnClick(1)" type="submit"><span class="lnr
                lnr-magnifier"></span></button>
                <div class="search-result" class="search-result" style="width: calc(100% - 48px)">
                  <ul id="result" class="result"></ul>
                </div>
              </div>
            </form>
            <!--            </div>-->
          </div>
        </div>

        <div class="col-lg-3 col-md-6">
          <div class="categories_menu">
            <div class="categories_title">
              <h2 class="categori_toggle">All Brands</h2>
            </div>
            <div class="categories_menu_toggle">
              <ul>
                  <?php
                  $brands = list_brand($conn);
                  foreach ($brands as $item) {
                      ?>
                    <li>
                      <a href="index.php?p=brand&slug=<?php echo $item['slug'] ?>"><?php echo $item['name'] ?>
                      </a>
                    </li>
                  <?php } ?>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <!--main menu start-->
          <div class="main_menu menu_position">
            <nav>
              <ul>
                  <?php
                  $categories = all_category($conn);
                  foreach ($categories as $item) {
                      ?>
                    <li>
                      <a <?php
                      if ($item['slug'] == 'home') {
                          echo 'href="index.php"';
                      }
                      if ($item['slug'] == 'about-us') {
                          echo 'href="index.php?p=about"';
                      }
                      if ($item['slug'] == 'products') {
                          echo 'href="index.php?p=shop-list&slug=dresses"';
                      }
                      if ($item['slug'] == 'contact-us') {
                          echo 'href="index.php?p=contact"';
                      }
                      ?>><?php echo $item['name'] ?>
                      </a>
                    </li>
                  <?php } ?>
              </ul>

            </nav>
          </div>
          <!--main menu end-->
        </div>
        <div class="col-lg-3">
          <div class="call-support">
            <p><a href="tel:(08)23456789">(08) 23 456 789</a> Customer Support</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  // initial ajax
  const queryUrl = 'model/product'
  // const keyword = document.getElementById('search-product')
  // const searchResult = document.getElementById('search-result')
  // const result = document.getElementById('result')

  const keyword = document.getElementsByClassName('search-product')
  const searchResult = document.getElementsByClassName('search-result')
  const result = document.getElementsByClassName('result')

  // const brandSelected = document.getElementById('categori2')
  let brandName = null

  const onSelectBrand = (select) => {
    brandName = select.value.toLowerCase()
  }

  function delay (fn, ms) {
    let timer = 0
    return function (...args) {
      clearTimeout(timer)
      timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
  }

  const inputHandler = (e, keyword, searchResult, result, brandName) => {
    if (keyword.contains(e.target)) {
      keyword.addEventListener('keyup', delay((e) => {
        const searchKeyword = e.target.value
        if (searchKeyword) {
          searchResult.classList.add('search-result-active')
        } else {
          searchResult.classList.remove('search-result-active')
        }
        ajax.onreadystatechange = function () {
          if (this.readyState === 4 && this.status === 200) {
            const response = JSON.parse(this.responseText)
            const ulList = document.querySelectorAll('#result li')
            for (let i = 0; li = ulList[i]; i++) {
              li.parentNode.removeChild(li)
            }
            if (response.length > 0) {
              response.map((val) => {
                result.innerHTML += `<li value="${val.id}"><a href="index.php?p=product-detail&id=${val.id}">${val.name}</a></li>`
              })
            } else {
              result.innerHTML = `<li value="" style="pointer-events: none;">No result to show</li>`
            }
          }
        }
        ajax.open('GET', `${queryUrl}/search-product.php?q=${searchKeyword}&brand=${brandName}`)
        ajax.send()
      }, 100))
    } else {
      searchResult.classList.remove('search-result-active')
    }
  }

  window.addEventListener('click', function (e) {
    for (let i = 0; i < keyword.length; i++) {
      keyword[i].addEventListener('keyup', inputHandler(e, keyword[i], searchResult[i], result[i], brandName))
    }
  })

  const onSearchBtnClick = (i) => {
    window.location = `index.php?p=search&q=${keyword[i].value}&brand=${brandName}`
  }

</script>
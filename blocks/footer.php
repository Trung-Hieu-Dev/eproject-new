<div class="footer_top">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-12 col-sm-7">
        <div class="widgets_container contact_us">
          <div class="footer_logo">
            <a href="index.php"><img src="./public/assets/img/logo/logo.png" alt=""></a>
          </div>
          <p class="footer_desc">We are a team of designers and developers creating high quality women's fashion products</p>
          <p><span>Address:</span> 4710-4890 Breckinridge USA</p>
          <p><span>Email:</span> <a href="#">demo@hasthemes.com</a></p>
          <p><span>Call us:</span> <a href="tel:(08)23456789">(08) 23 456 789</a></p>
        </div>
      </div>
      <div class="col-lg-2 col-md-3 col-sm-5">
        <div class="widgets_container widget_menu">
          <h3>Information</h3>
          <div class="footer_menu">

            <ul>
              <?php
              $categories = all_category($conn);
              foreach ($categories as $item) {
              ?>
                <li>
                  <a <?php
                      if ($item['slug'] == 'home') {
                        echo 'class="active"';
                      } else {
                        echo '';
                      }
                      ?> <?php
                          if ($item['slug'] == 'home') {
                            echo 'href="index.php"';
                          }
                          if ($item['slug'] == 'about-us') {
                            echo 'href="index.php?p=about"';
                          }
                          if ($item['slug'] == 'products') {
                            echo 'href="index.php?p=shop-list&slug=dresses"';
                          }
                          if ($item['slug'] == 'contact-us') {
                            echo 'href="index.php?p=contact"';
                          }
                          ?>><?php echo $item['name'] ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-2 col-md-3 col-sm-4">
        <div class="widgets_container widget_menu">
          <h3>All Brands</h3>
          <div class="footer_menu">
            <ul>
              <?php
              $brands = list_brand($conn);
              foreach ($brands as $item) {
              ?>
                <li>
                  <a href="index.php?p=brand&slug=<?php echo $item['slug'] ?>"><?php echo $item['name'] ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-sm-8">
        <div class="widgets_container widget_newsletter">
          <h3>ApTech Computer Education</h3>
          <div class="logo">
            <a href="#">
              <img src="https://aptechvietnam.com.vn/sites/all/themes/aptech_news_2/logo.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="footer_bottom">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-12 col-md-12">
        <div class="copyright_area">
          <p>ApTech Instructors: <a href="#">Mr.Vu Quoc Tuan</a> . Design by <a href="#">ApTech 2011L</a></p>
        </div>
      </div>
      
    </div>
  </div>
</div>
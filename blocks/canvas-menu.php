<div class="off_canvars_overlay">

</div>
<div class="offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <a href="javascript:void(0)"><i class="icon-menu"></i></a>
                </div>
                <div class="offcanvas_menu_wrapper">
                    <div class="canvas_close">
                        <a href="javascript:void(0)"><i class="icon-x"></i></a>
                    </div>

                    <div class="call-support">
                        <p><a href="tel:(08)23456789">(08) 23 456 789</a> Customer Support</p>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">

                            <?php
                            $categories = all_category($conn);
                            foreach ($categories as $item) {
                            ?>
                                <li class="menu-item-has-children <?php
                                                                    if ($item['name'] == 'Home') {
                                                                        echo 'class="active"';
                                                                    } else {
                                                                        echo '';
                                                                    }
                                                                    ?>">
                                    <a <?php
                                        if ($item['id'] == 1) {
                                            echo 'href="index.php"';
                                        }
                                        if ($item['id'] == 2) {
                                            echo 'href="index.php?p=about"';
                                        }
                                        if ($item['id'] == 3) {
                                            echo 'href="index.php?p=shop-list"';
                                        }
                                        if ($item['id'] == 4) {
                                            echo 'href="index.php?p=contact"';
                                        }
                                        ?>><?php echo $item['name'] ?>
                                    </a>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>
                    <div class="offcanvas_footer">
                        <span><a href="#"><i class="fa fa-envelope-o"></i> trunghieu.aptech2011l@gmail.com</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
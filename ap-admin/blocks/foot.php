<!-- jQuery -->
<script src="./public/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="./public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- product-feature & status button: bs-custom-file-input -->
<script src="./public/assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<!-- Bootstrap Switch -->
<script src="./public/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- DataTables  & Plugins -->

<!-- Product: DataTables -->
<script src="./public/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="./public/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="./public/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<!-- Product: DataTables -->
<!-- Tempusdominus Bootstrap 4 -->
<script src="../../plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- dropzonejs -->
<script src="../../plugins/dropzone/min/dropzone.min.js"></script>
<script src="./public/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="./public/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="./public/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="./public/assets/plugins/jszip/jszip.min.js"></script>
<script src="./public/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="./public/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="./public/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="./public/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="./public/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!--Sweet alert-->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- AdminLTE App -->
<script src="./public/assets/dist/js/adminlte.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="./public/assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- myscript.js -->
<script src="./public/assets/dist/js/myscript.js"></script>

<!-- product- feature  & status button bootstrap -->
<script type="text/javascript">
  $(document).ready(function () {
    bsCustomFileInput.init()
// product: datatable at product-index.php
    $('#datatable').DataTable({
      'responsive': true,
      'autoWidth': false,
    })
  })

  // product: if no image, show no-image image
  function imgError (image) {
    image.onerror = ''
    image.src = '../public/assets/image/no-image.png'
    return true
  }
</script>
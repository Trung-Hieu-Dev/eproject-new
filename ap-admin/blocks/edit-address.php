<div class="card card-default address-detail-container"
     id="my-card-widget">
    <div class="card-header">
        <div class="form-group">
            <div class="address-title-container">
                <div class="address-title">
                    <label>Address detail</label>
                </div>
                <div class="card-tools">
                    <button type="button"
                            class="btn btn-tool"
                            data-card-widget="collapse">
                        Edit address
                    </button>
                </div>
            </div>
            <input type="text" class="form-control" disabled
                   placeholder="Please enter user address details."
                   value="<?php display_address($conn, $address_detail, $ward, $district, $province); ?>">
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row ">
            <div class="col-6">
                <div class="form-group">
                    <label>Select Province/City</label>
                    <select id="province" class="form-control" name="province_city"
                            onchange="addressCodeSelect('province')">
                        <option value="" selected disabled>Select</option>
                        <?php
                        if (isset($_POST["province_city"])) {
                            keep_province_address_option($conn, $_POST["province_city"]);
                        } else {
                            keep_province_address_option($conn, $province);
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>Select District</label>
                    <select id="district" class="form-control" name="district"
                            onchange="addressCodeSelect('district')">
                        <option value="" selected disabled>Select</option>
                        <?php
                        if (isset($_POST["district"]) && isset($_POST["province_city"])) {
                            keep_district_address_option($conn, $_POST["province_city"], $_POST["district"]);
                        } else {
                            keep_district_address_option($conn, $province, $district);
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>Select Ward</label>
                    <select id="ward" class="form-control" name="ward">
                        <option value="" selected disabled>Select</option>
                        <?php
                        if (isset($_POST["district"]) && isset($_POST["province_city"]) && isset($_POST["ward"])) {
                            keep_ward_address_option($conn, $_POST["district"], $_POST["ward"]);
                        } else {
                            keep_ward_address_option($conn, $district, $ward);
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>House number and street name</label>
                    <input type="text" class="form-control" name="address_detail"
                           placeholder="Please enter user address detail" <?php keep_value_input('address_detail', $address_detail);
                    ?>>
                </div>
            </div>
        </div>
    </div>
</div>
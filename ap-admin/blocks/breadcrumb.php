<?php
$breadcrumb_item = null;
if (isset($_GET["module"])) {
    if (!isset($_GET["action"]) || $_GET["action"] == 'index') {
        $breadcrumb_item = "Manage " . ucfirst($_GET["module"]);
    } else {
        $breadcrumb_item = ucfirst($_GET["action"]) . " " . ucfirst($_GET["module"]);
    }
}
?>
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1 class="m-0"><?php echo $breadcrumb_item ?></h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active"><?php echo $breadcrumb_item ?></li>
      </ol>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</div>
<!-- /.container-fluid -->
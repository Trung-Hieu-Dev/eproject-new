<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>ApTech | eProject</title>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="./public/assets/plugins/fontawesome-free/css/all.min.css" />
<!-- Ioions?? -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="./public/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="./public/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="./public/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="./public/assets/dist/css/adminlte.min.css" />
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="../../plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
<!-- dropzonejs -->
<link rel="stylesheet" href="../../plugins/dropzone/min/dropzone.min.css">
<!--My style-->
<link rel="stylesheet" href="./public/assets/dist/css/mystyles.css" />
<!-- Bootstrap4 Duallistbox -->
<link rel="stylesheet" href="./public/assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
<!-- CK Editor -->
<script type="text/javascript" src="./public/assets/plugins/ckeditor/ckeditor.js"></script>
<!-- CK Finder -->
<script type="text/javascript" src="./public/assets/plugins/ckfinder/ckfinder.js"></script>

<script type="text/javascript">
  function checkDelete (msg) {
    if (window.confirm(msg)) {
      return true
    }
    return false
  }
</script>
<!-- Product -->

<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <img src="./public/assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: 0.8;" />
    <span class="brand-text font-weight-light">Aptech eProject</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex user-customize">
      <div class="image">
        <img src="<?php echo "./public/assets/user_image/" . $_SESSION["user_profile"]["avatar_img"] ?>"
             class="img-circle elevation-2 img-custom" alt="<?php echo "user-" . rand() ?>"
             onerror="this.src='./public/assets/images/no-image.png'" />
      </div>
      <div class="info">
        <div class="word-wrap"
        "><?php echo
          $_SESSION["user_profile"]["full_name"] ?></div>
      <a type="button" href="logout.php" class="logout-customize confirmLogout">Logout</a>
    </div>
  </div>

  <!-- SidebarSearch Form -->
  <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
      <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
      <div class="input-group-append">
        <button class="btn btn-sidebar">
          <i class="fas fa-search fa-fw"></i>
        </button>
      </div>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column menu-open" data-widget="treeview" role="menu"
        data-accordion="false">
      <li class="nav-item">
        <a href="index.php?module=home" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Dashboard
            <span class="right badge badge-danger">New</span>
          </p>
        </a>
      </li>

      <!-- Category -->
      <li class="nav-item <?php active_menu('category', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=category"
           class="nav-link">
          <i class="nav-icon fas fa-align-justify"></i>
          <p>
            Category
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=category&action=create"
               class="nav-link <?php active_menu('category', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create Category</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=category&action=index"
               class="nav-link <?php active_menu('category', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage Category</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.category -->

      <!-- Product -->
      <li class="nav-item <?php active_menu('product', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=product" class="nav-link">
          <i class="nav-icon fas fa-box"></i>
          <p>
            Product
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=product&action=create"
               class="nav-link <?php active_menu('product', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create Product</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=product&action=index"
               class="nav-link <?php active_menu('product', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage Product</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.Product -->

      <!-- Branch -->
      <li class="nav-item <?php active_menu('brand', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=brand" class="nav-link">
          <i class="nav-icon fab fa-artstation"></i>
          <p>
            Brand
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=brand&action=create"
               class="nav-link <?php active_menu('brand', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create Brand</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=brand&action=index"
               class="nav-link <?php active_menu('brand', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage Brand</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.branch -->

      <!--User-->
      <li class="nav-item <?php active_menu('user', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=user" class="nav-link">
          <i class="nav-icon fas fa-users"></i>
          <p>
            User
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=user&action=create" class="nav-link <?php active_menu('user', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create User</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=user&action=index"
               class="nav-link <?php active_menu('user', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage User</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.user -->

      <!-- Slider -->
      <li class="nav-item <?php active_menu('slider', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=slider" class="nav-link">
          <i class="nav-icon fab fas fa-images"></i>
          <p>
            Slider
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=slider&action=create"
               class="nav-link <?php active_menu('slider', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create Slider</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=slider&action=index"
               class="nav-link <?php active_menu('slider', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage Slider</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.slider -->

      <!-- banner -->
      <li class="nav-item <?php active_menu('banner', ['index', 'create', 'edit'], 'menu_open'); ?>">
        <a href="index.php?module=banner" class="nav-link">
          <i class="nav-icon fab fas fa-flag "></i>
          <p>
            Banner
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="index.php?module=banner&action=create"
               class="nav-link <?php active_menu('banner', ['create']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Create banner</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="index.php?module=banner&action=index"
               class="nav-link <?php active_menu('banner', ['index', 'edit']); ?>">
              <i class="far fa-circle nav-icon"></i>
              <p>Manage banner</p>
            </a>
          </li>
        </ul>
      </li>
      <!-- /.banner -->

    </ul>
  </nav>
  <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
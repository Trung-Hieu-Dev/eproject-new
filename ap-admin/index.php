<?php
session_start();
ob_start();

if (!$_SESSION["user_profile"]) {
    header("location:login.php");
    exit();
}

include '../core/config.php';
include '../core/connect.php';
include '../core/function.php';

?>


<!DOCTYPE html>

<html lang="en">

<head>
    <?php include './blocks/head.php' ?>
</head>

<body class="hold-transition sidebar-mini dark-mode">
<div class="wrapper">
  <!-- Navbar -->
    <?php include './blocks/navbar.php' ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
    <?php include './blocks/sidebar.php' ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <?php include './blocks/breadcrumb.php' ?>
        </div>
        <!-- /.content-header -->
          <?php
          if (isset($_GET["module"])) {
              $module = $_GET["module"];
              $action = null;

              if (isset($_GET["action"])) {
                  $action = $_GET["action"];
              } else {
                  $action = 'index';
              }

              if (file_exists("./modules/$module/$action.php")) {
                  include "./model/$module.php";
                  include "./modules/$module/$action.php";
              } else {
                  header("location:error.php");
                  exit();
              }

          } else {
              include './modules/user/index.php';
          }

          ?>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
    <?php include './blocks/rightside.php' ?>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
    <?php include './blocks/footer.php' ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<?php include './blocks/foot.php' ?>
</body>

</html>
<?php
$errors = array();

if (isset($_POST["submit"])) {
    if (empty($_POST["name"])) {
        $errors[] = "Please enter your slider name";
    }

    if (empty($_POST["slug"])) {
        $errors[] = "Please enter your slug";
    }

    if (empty($_FILES["image"]["name"])) {
        $errors[] = "Please choose your slider logo";
    }

    if (!check_ext($_FILES["image"]["type"])) {
        $errors[] = "This is not image file";
    }

    if (empty($errors)) {
        $file = change_name_file($_FILES["image"]["name"]);

        $data = array(
            'name' => $_POST["name"],
            'slug' => $_POST["slug"],
            'image' => $file,
            'status' => (isset($_POST["status"]) && ($_POST["status"] == 'on')) ? 1 : 0
        );

        if (check_slider_exist($conn, $data)) {
            create_slider($conn, $data);

            move_uploaded_file($_FILES["image"]["tmp_name"], './public/assets/brand_image/' . $file);


            header("location:index.php?module=slider&action=index");
            exit();
        } else {
            $errors[] = "This brand is existed";
        }
    }
}

?>

<!-- Alert start -->
<?php if (!empty($errors)) { ?>

    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
        <?php
        foreach ($errors as $error) {
            echo '<li>' . $error . '</li>';
        }
        ?>
    </div>

<?php } ?>
<!-- /.alert -->


<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Input Your Information</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="form-group">
                        <label>Slider Name</label>
                        <input type="text" class="form-control convert_slug" name="name" data-slug="slug" placeholder="Please enter your category name" <?php keep_value_input('name'); ?>>
                    </div>
                    <div class="form-group">
                        <label>Slug</label>
                        <input type="text" class="form-control" name="slug" placeholder="Please enter your slug" <?php keep_value_input('slug'); ?>>
                    </div>
                    <div class="form-group">
                        <label>Slider Logo</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="custom-file" name="image">
                            <label class="custom-file-label" for="custom-file">Choose file</label>
                        </div>
                    </div>

                    <input type="checkbox" class="bootstrap_switch" name="status" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
</div>
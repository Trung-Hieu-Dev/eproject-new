<?php
ob_start();
$errors = array();
$success = false;
$edit_myself = null;
$id = $_GET['id'];
$is_permission = true;

if ($_SESSION["user_profile"]["id"] == $id) {
    $edit_myself = true;
} else {
    $edit_myself = false;
}

if (!isset($_GET['id'])) {
    header("location:index.php?module=user");
    exit();
}

if ($id == "") {
    header("location:index.php?module=user");
    exit();
}

$errors = array();
$old_data = get_user_info($conn, $id);
$province = $old_data[0]["province_city"];
$district = $old_data[0]["district"];
$ward = $old_data[0]["ward"];
$address_detail = $old_data[0]["address_detail"];
$status_check = $old_data[0]["status"] == 1 ? 'checked' : '';

if ($_SESSION["user_profile"]["level"] == 1 && ($old_data[0]["level"] == 0 || ($old_data[0]["level"]
            == 1 && $edit_myself == false))) {
    echo '<script>
alert("You do not have permission to edit this user.")
window.location.href="index.php?module=user"
</script>';
    exit();
}

if (isset($_POST['submit'])) {
    if (empty($_POST["full_name"])) {
        $errors[] = "Please enter your full name.";
    } else {
        $name = $_POST["full_name"];
        if (!preg_match("/^([a-zA-Z' ]+)$/", $name)) {
            $errors[] = "Full name does not contain number or special characters.";
        }
    }

    if (empty($_POST["phone"])) {
        $errors[] = "Please enter phone number.";
    }

    if (!is_numeric($_POST["phone"])) {
        $errors[] = "Phone number must be a number.";
    }

    if (strlen($_POST["phone"]) > 11 || strlen($_POST["phone"]) < 10) {
        $errors[] = "Phone number must be between 10 and 11 characters.";
    }

    if (!empty($_POST["province_city"])) {
        if (empty($_POST["district"])) {
            $errors[] = "Please select a district.";
        }
        if (empty($_POST["ward"])) {
            $errors[] = "Please select a ward.";
        }
        if (empty($_POST["address_detail"])) {
            $errors[] = "Please address details (Apartment No., Street).";
        }
    }

    if (!empty($_POST["password"])) {
        $password = $_POST["password"];
        if (strlen($password) < 8) {
            $errors[] = "Password must be at least 8 characters.";
        }
        if (!preg_match("/\d/", $password)) {
            $errors[] = "Password must be contain at least one number.";
        }
        if (!preg_match("/[A-Z]/", $password)) {
            $errors[] = "Password muse be contain at least one uppercase letter.";
        }
        if (!preg_match("/[a-z]/", $password)) {
            $errors[] = "Password must be contain at least one lowercase letter.";
        }
        if (!preg_match("/\W/", $password)) {
            $errors[] = "Password must be contain at least one special character.";
        }
        if (preg_match("/\s/", $password)) {
            $errors[] = "Password should not contain any white space.";
        }
        if (empty($_POST["confirm_password"])) {
            $errors[] = "Please enter a confirm password.";
        } else {
            if ($_POST['password'] !== $_POST['confirm_password']) {
                $errors[] = "Password does not match.";
            }
        }
    }

    if (empty($errors)) {
        $data = array(
            "status" => (isset($_POST["status"]) && ($_POST["status"] == 'on')) ? 1 : 0,
            "full_name" => $_POST["full_name"],
            "phone" => $_POST["phone"],
            "address_detail" => $_POST["address_detail"],
            "user_id" => $id
        );

        if (!$edit_myself) {
            $data["level"] = $_POST["level"];
        }

        if (isset($_POST["password"])) {
            $data["password"] = $_POST["password"];
        }

        if (isset($_POST["province_city"])) {
            $data["province_city"] = $_POST["province_city"];
            $data["district"] = $_POST["district"];
            $data["ward"] = $_POST["ward"];
        }

        if (!empty($_FILES['avatar_img']["name"])) {
            $imageFileName = change_name_file($_FILES['avatar_img']["name"]);
            if (!checkExt($_FILES['avatar_img']["name"])) {
                $errors[] = "It is not an image file";
            } else {
                $data["avatar_img"] = $imageFileName;
                update_user($conn, $data, $success);
            }
        } else {
            $data["avatar_img"] = $old_data[0]["avatar_img"];
            update_user($conn, $data, $success);
        }

        if ($success) {
            if (!empty($_FILES['avatar_img']["name"])) {
                move_uploaded_file($_FILES['avatar_img']["tmp_name"], './public/assets/user_image/' . $data["avatar_img"]);
            }
            header("location:index.php?module=user");
            exit();
        }
    }
}
if (isset($_POST['cancel'])) {
    header("location:index.php?module=user");
    exit();
}
?>

<!-- Alert start -->
<?php if (!empty($errors)) { ?>
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
      <?php
      foreach ($errors as $error) {
          echo '<li>' . $error . '</li>';
      }
      ?>
  </div>
<?php } ?>
<!-- /.alert -->

<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Edit Your Information</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Full name</label>
            <input type="text" class="form-control" name="full_name"
                   placeholder="Please enter user full name" <?php keep_value_input('full_name', $old_data[0]["full_name"]);
            ?>>
          </div>
            <?php
            if ($address_detail && $ward && $district && $province) {
                include __DIR__ . '/../../blocks/edit-address.php';
            } else {
                include __DIR__ . '/../../blocks/edit-create-non-address.php';
            }
            ?>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone"
                   placeholder="Please enter user phone number" <?php keep_value_input('phone', $old_data[0]["phone"]); ?>>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" disabled
                   placeholder="Please enter user email" <?php keep_value_input('email', $old_data[0]["email"]); ?>>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password"
                   placeholder="Please enter user password">
          </div>
          <div class="form-group">
            <label>Confirm password</label>
            <input type="password" class="form-control" name="confirm_password"
                   placeholder="Please enter confirmation password">
          </div>
            <?php if (!$edit_myself) { ?>
              <div class="form-group">
                <label>Level</label>
                <select class="form-control" name="level">
                  <option value="2" <?php keep_value_option('level', 2, $old_data[0]["level"]); ?>>Customer</option>
                  <option value="1" <?php keep_value_option('level', 1, $old_data[0]["level"]); ?>>Admin</option>
                  <option value="0" <?php keep_value_option('level', 0, $old_data[0]["level"]); ?>>Super Admin</option>
                </select>
              </div>
            <?php } ?>
          <div class=" form-group">
            <label>Using Image</label>
            <img class="d-block"
                 src="./public/assets/user_image/<?php echo $old_data[0]["avatar_img"] ?>"
                 alt="<?php echo "user-" . rand() ?>"
                 onerror="this.src='./public/assets/images/no-image.png'"
                 width="200px">
          </div>
          <div class="form-group">
            <label>User avatar</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="custom-file" name="avatar_img">
              <label class="custom-file-label" for="custom-file">Choose file</label>
            </div>
          </div>
          <input type="checkbox" class="bootstrap_switch"
                 name="status" data-bootstrap-switch
                 data-off-color="danger" data-on-color="success"
              <?php echo $status_check ?>
          >
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
          <button type="submit" name="cancel" class="btn btn-default float-right confirmCancel">Cancel</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
  </div>
</div>

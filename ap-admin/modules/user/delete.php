<?php
if (!isset($_GET['id'])) {
    header("location:index.php?module=user");
    exit();
} else {
    $id = $_GET["id"];
    if ($id && $id != "") {
        $old_data = get_user_info($conn, $id);

        if ($old_data[0]["level"] == 0 || ($_SESSION["user_profile"]["level"] != 0 && $old_data[0]["level"] == 1)) {
            echo '<script>
alert("You do not have permission to delete this user.")
window.location.href="index.php?module=user"
</script>';
            exit();
        }

        if (file_exists('./public/assets/user_image/' . $old_data[0]["avatar_img"])) {
            unlink('./public/assets/user_image/' . $old_data[0]["avatar_img"]);
        }

        delete_user($conn, $id);
    }
    header("location:index.php?module=user");
    exit();
}
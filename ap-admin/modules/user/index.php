<?php
$data = get_all_user_info($conn);
?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">List Category</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Num</th>
            <th>Full name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Level</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          <?php foreach ($data as $key => $item) { ?>
            <tr>
              <td><?php echo $key + 1 ?></td>
              <td><?php echo $item["full_name"] ?></td>
              <td><?php echo $item["email"] ?></td>
              <td><?php echo $item["phone"] ?></td>
              <td><?php
                  switch ($item["level"]) {
                      case 0:
                          echo '<div class="badge badge-danger">Super Admin</div>';
                          break;
                      case 1:
                          echo '<div class="badge badge-warning">Admin</div>';
                          break;
                      default:
                          echo '<div class="badge badge-success">Customer</div>';
                  }
                  ?></td>
              <td><?php
                  switch ($item["status"]) {
                      case 0:
                          echo '<div class="badge badge-success">Disable</div>';
                          break;
                      default:
                          echo '<div class="badge badge-info">Active</div>';
                  }
                  ?></td>
              <td>
                <a class="confirmDelete badge badge-danger"
                   href="index.php?module=user&action=delete&id=<?php echo $item["user_id"] ?>">Delete</a> |
                <a class="badge badge-warning"
                   href="index.php?module=user&action=edit&id=<?php echo $item["user_id"] ?>">Edit</a>
              </td>
            </tr>
          <?php } ?>
          </tbody>
          <tfoot>
          <tr>
            <th>Num</th>
            <th>Full name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Level</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
      </div>
    </div>
    </tr>
    </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
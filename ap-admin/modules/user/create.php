<?php
ob_start();
$errors = array();
$success = false;

if (isset($_POST["submit"])) {
    if (empty($_POST["full_name"])) {
        $errors[] = "Please enter your full name.";
    } else {
        $name = $_POST["full_name"];
        if (!preg_match("/^([a-zA-Z' ]+)$/", $name)) {
            $errors[] = "Full name does not contain number or special characters.";
        }
    }

    if (empty($_POST["province_city"])) {
        $errors[] = "Please select a province or a city.";
    }

    if (empty($_POST["district"])) {
        $errors[] = "Please select a district.";
    }

    if (empty($_POST["ward"])) {
        $errors[] = "Please select a ward.";
    }

    if (empty($_POST["address_detail"])) {
        $errors[] = "Please address details (Apartment No., Street).";
    }

    if (empty($_POST["phone"])) {
        $errors[] = "Please enter phone number.";
    } else {
        if (!is_numeric($_POST["phone"])) {
            $errors[] = "Phone number must be a number.";
        }
        if (strlen($_POST["phone"]) > 11) {
            $errors[] = "Phone number must be between 10 and 11 characters.";
        }
    }

    if (empty($_POST["email"])) {
        $errors[] = "Please enter a email address.";
    } else {
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Invalid email address.";
        }
    }

    if (empty($_POST["password"])) {
        $errors[] = "Please enter a password.";
    } else {
        $password = $_POST["password"];
        if (strlen($password) < 8) {
            $errors[] = "Password must be at least 8 characters.";
        }
        if (!preg_match("/\d/", $password)) {
            $errors[] = "Password must be contain at least one number.";
        }
        if (!preg_match("/[A-Z]/", $password)) {
            $errors[] = "Password muse be contain at least one uppercase letter.";
        }
        if (!preg_match("/[a-z]/", $password)) {
            $errors[] = "Password must be contain at least one lowercase letter.";
        }
        if (!preg_match("/\W/", $password)) {
            $errors[] = "Password must be contain at least one special character.";
        }
        if (preg_match("/\s/", $password)) {
            $errors[] = "Password should not contain any white space.";
        }
    }

    if (empty($_POST["confirm_password"])) {
        $errors[] = "Please enter a confirm password.";
    } else {
        if ($_POST['password'] !== $_POST['confirm_password']) {
            $errors[] = "Password does not match.";
        }
    }

    if (empty($errors)) {
        $data = array(
            "email" => $_POST["email"],
            "password" => $_POST["password"],
            "level" => $_POST["level"],
            "status" => $_POST["status"] === 'on' ? 1 : 0,
            "full_name" => $_POST["full_name"],
            "phone" => $_POST["phone"],
            "province_city" => $_POST["province_city"],
            "district" => $_POST["district"],
            "ward" => $_POST["ward"],
            "address_detail" => $_POST["address_detail"]
        );

        if (!empty($_FILES['avatar_img']["name"])) {
            $imageFileName = change_name_file($_FILES['avatar_img']["name"]);
            $data["avatar_img"] = $imageFileName;
        }

        if (!check_user_exists($conn, $data)) {
            create_user($conn, $data, $success);
        } else {
            $errors[] = 'Email is already exists';
        }

        if ($success) {
            move_uploaded_file($_FILES['avatar_img']["tmp_name"], './public/assets/user_image/' . $imageFileName);
            header("location:index.php?module=user");
            exit();
        }
    }
}
if (isset($_POST['cancel'])) {
    header("location:index.php?module=user");
    exit();
}
?>

<!-- Alert start -->
<?php if (!empty($errors)) { ?>
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
      <?php
      foreach ($errors as $error) {
          echo '<li>' . $error . '</li>';
      }
      ?>
  </div>
<?php } ?>
<!-- /.alert -->

<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Input Your Information</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Full name</label>
            <input type="text" class="form-control" name="full_name"
                   placeholder="Please enter user full name" <?php keep_value_input('full_name'); ?>>
          </div>
          <!--          address create-->
            <?php include __DIR__ . '/../../blocks/edit-create-non-address.php' ?>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone"
                   placeholder="Please enter user phone number" <?php keep_value_input('phone'); ?>>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email"
                   placeholder="Please enter user email" <?php keep_value_input('email'); ?>>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password"
                   placeholder="Please enter user password">
          </div>
          <div class="form-group">
            <label>Confirm password</label>
            <input type="password" class="form-control" name="confirm_password"
                   placeholder="Please enter confirmation password">
          </div>
          <div class="form-group">
            <label>Level</label>
            <select class="form-control" name="level">
                <?php if ($_SESSION["user_profile"]["level"] == 0) { ?>
                  <option value="2" <?php keep_value_option('level', 2); ?>>Customer</option>
                  <option value="1" <?php keep_value_option('level', 1); ?>>Admin</option>
                  <option value="0" <?php keep_value_option('level', 0); ?>>Super Admin</option>
                <?php } else { ?>
                  <option value="2" <?php keep_value_option('level', 2); ?>>Customer</option>
                <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>User avatar</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" id="custom-file" name="avatar_img">
              <label class="custom-file-label" for="custom-file">Choose file</label>
            </div>
          </div>

          <input type="checkbox" class="bootstrap_switch" name="status" checked data-bootstrap-switch
                 data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
          <button type="submit" name="cancel" class="btn btn-default float-right confirmCancel">Cancel</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
  </div>
</div>

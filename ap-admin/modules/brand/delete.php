<?php

if (!isset($_GET["id"])) {
    header("location:index.php?module=brand&action=index");
    exit();
}


if(isset($_GET["id"])) {
    $id = $_GET["id"];
    
    settype($id, 'int');

    if($id != 0) {
        $old_data = get_brand($conn, $id);
        
        if(file_exists('./public/assets/brand_image/' . $old_data["image"])) {
            unlink('./public/assets/brand_image/'.$old_data["image"]);
        }

        delete_brand($conn, $id);

        header("location:index.php?module=brand");
        exit();
        
    } else {
        echo 'Can not delete';
    }
}

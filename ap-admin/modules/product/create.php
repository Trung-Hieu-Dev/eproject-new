<?php
$errors = array();
$parent_category = get_all_category($conn);
$brand = get_all_brand($conn);

if (isset($_POST["create"])) {

    if (empty($_POST["name"])) {
        $errors[] = "Please input product name";
    }

    if (empty($_POST["price"])) {
        $errors[] = "Please input price";
    }

    if (!is_numeric($_POST["price"])) {
        $errors[] = "Price must be number";
    }

    if (empty($_POST["sale_price"])) {
        $errors[] = "Please input sale price";
    }

    if (!is_numeric($_POST["sale_price"])) {
        $errors[] = "Sale price must be number";
    }

    if (empty($_POST["intro"])) {
        $errors[] = "Please input introduction";
    }
    // Insert image file
    if (empty($_FILES["image"]["name"])) {
        $errors[] = "Please input image";
    }
    // Check it is image or not
    if (!checkExt($_FILES["image"]["name"])) {
        $errors[] = "It is not an image";
    }
    // When no error, add data into database
    if (empty($errors)) {
        $file = change_name_file($_FILES["image"]["name"]);

        $data = array(
            'name' => $_POST["name"],
            'price' => $_POST["price"],
            'sale_price' => $_POST["sale_price"],
            'intro' => $_POST["intro"],
            'content' => $_POST["content"],
            'image' => $file,
            'status' => $_POST["status"],
            'featured' => $_POST["featured"],
            'category_id' => $_POST["category_id"],
            'brand_id' => $_POST["brand_id"]

        );

        if (check_product_exist($conn, $data)) {
            create_product($conn, $data);
            // upload image file on server & format file
            move_uploaded_file($_FILES["image"]["tmp_name"], '../public/assets/image-product/' . $file);

            header("location:index.php?module=product");
            exit();
        } else {
            $errors[] = "This product name is existed";
        }
    }
}
?>
<?php if (!empty($errors)) { ?>
  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Error!</h5>
    <ul>
        <?php foreach ($errors as $error) { ?>
          <li><?php echo $error ?></li>
        <?php } ?>
    </ul>
  </div>
<?php } ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Input Your Information</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <!-- Accept to up image : enctype="multipart/form-data" -->
      <form method="POST" action="" enctype="multipart/form-data">
        <div class="card-body">
          <div class="form-group">
            <label>Brand</label>
            <select class="form-control" name="brand_id">
                <?php recursive_option($brand, $_POST["brand_id"]) ?>
            </select>
          </div>
          <div class="form-group">
            <label>Category</label>
            <select class="form-control" name="category_id">
              <!-- Parent:In category database, product will be at no.3. Just show product only ) -->
                <?php recursive_option($parent_category, $_POST["category_id"], 3) ?>
            </select>
          </div>
          <div class="form-group">
            <label>Product name</label>
            <input type="text" name="name" class="form-control" placeholder="Please input product name" <?php
            if (isset($_POST["name"])) {
                echo 'value="' . $_POST["name"] . '"';
            }
            ?>>
          </div>
          <div class="form-group">
            <label>Price</label>
            <input type="text" name="price" class="form-control" placeholder="Please input product price" <?php
            if (isset($_POST["price"])) {
                echo 'value="' . $_POST["price"] . '"';
            }
            ?>>
          </div>
          <div class="form-group">
            <label>Sale price</label>
            <input type="text" name="sale_price" class="form-control" placeholder="Please input sale price" <?php
            if (isset($_POST["sale_price"])) {
                echo 'value="' . $_POST["sale_price"] . '"';
            }
            ?>>
          </div>
          <div class="form-group">
            <label>Intro</label>
            <textarea class="form-control" name="intro"><?php
                if (isset($_POST["intro"])) {
                    echo $_POST["intro"];
                }
                ?></textarea>
            <script>
              CKEDITOR.replace('intro', {
                filebrowserBrowseUrl: 'http://localhost:8888/Eproject-Madam-Boutique/final-project/ap-admin/public/assets/plugins/ckfinder/ckfinder.html',
                filebrowserUploadUrl: 'http://localhost:8888/Eproject-Madam-Boutique/final-project/ap-admin/public/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
              })
            </script>
          </div>
          <div class="form-group">
            <label>Content</label>
            <textarea class="form-control" name="content"><?php
                if (isset($_POST["content"])) {
                    echo $_POST["content"];
                }
                ?></textarea>
            <script>
              CKEDITOR.replace('content', {
                filebrowserBrowseUrl: 'http://127.0.0.1/Eproject-Madam-Boutique/final-project/ap-admin/public/assets/plugins/ckfinder/ckfinder.html',
                filebrowserUploadUrl: 'http://127.0.0.1/Eproject-Madam-Boutique/final-project/ap-admin/public/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
              })
            </script>
          </div>
          <div class="form-group">
            <label>Image</label>
            <div class="custom-file">
              <input type="file" name="image" class="custom-file-input" id="customFile">
              <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" name="status">
              <option value="1">On</option>
              <option value="0">Off</option>
            </select>
          </div>
          <div class="form-group">
            <label>Feature</label>
            <select class="form-control" name="featured">
              <option value="0">Off</option>
              <option value="1">On</option>
            </select>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="create" class="btn btn-info">Add</button>
          <button type="reset" class="btn btn-default float-right">Delete</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
  </div>
</div>
<?php
// Show content need to edit, need to check id-> list
if (!isset($_GET["id"])) {
    header("location:index.php?module=product&action=index");
    exit();
} else {
    $errors = array();
    $id = $_GET["id"];
    $parent_category = get_all_category($conn);
    $brand = get_all_brand($conn);
    $old = get_one_product($conn, $id);
    $recursive = data_recursive($conn);

    if (!check_product_id($conn, $id)) {
        header("location:index.php?module=product&action=index");
        exit();
    }
    // Get product out thru ID
    $product = get_product($conn, $id);

    if (isset($_POST["edit"])) {

        if (empty($_POST["name"])) {
            $errors[] = "Please input product name";
        }

        if (empty($_POST["price"])) {
            $errors[] = "Please input product price";
        }

        if (!is_numeric($_POST["price"])) {
            $errors[] = "Product price must be a number";
        }

        if (empty($_POST["sale_price"])) {
            $errors[] = "Please input Sale price";
        }

        if (!is_numeric($_POST["sale_price"])) {
            $errors[] = "Sale price must be a number";
        }

        if (empty($_POST["intro"])) {
            $errors[] = "Please input introduction";
        }

        if (empty($errors)) {

            $data = array(
                'name' => $_POST["name"],
                'price' => $_POST["price"],
                'sale_price' => $_POST["sale_price"],
                'intro' => $_POST["intro"],
                'content' => $_POST["content"],
                'status' => $_POST["status"],
                "featured" => $_POST["featured"],
                "category_id" => $_POST["category_id"],
                "brand_id" => $_POST["brand_id"],
                "id" => $id
            );
            // Check product exist before adding, if it si ok, then edit
            if (check_product_exist($conn, $data, true)) {
                // If no new image, use old image. If insert new image, check image in correct format or not then store in database
                if (!empty($_FILES["image"]["name"])) {
                    if (!checkExt($_FILES["image"]["name"])) {
                        $errors[] = "It is not an image file";
                    } else {
                        $file = change_name_file($_FILES["image"]["name"]);
                        // New image
                        $data["image"] = $file;

                        move_uploaded_file($_FILES["image"]["tmp_name"], '../public/assets/image-product/' . $file);

                        edit_product($conn, $data);
                        header("location:index.php?module=product");
                        exit();
                    }
                } else {
                    // Old image
                    $data["image"] = $product["image"];
                    edit_product($conn, $data);
                    header("location:index.php?module=product");
                    exit();
                }
            } else {
                $errors[] = "Product name is existed";
            }
        }
    }
    ?>
    <?php if (!empty($errors)) { ?>
    <div class="alert alert-danger alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <h5><i class="icon fas fa-ban"></i> Error!</h5>
      <ul>
          <?php foreach ($errors as $error) { ?>
            <li><?php echo $error ?></li>
          <?php } ?>
      </ul>
    </div>
    <?php } ?>
  <form method="POST" action="" enctype="multipart/form-data">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Input Your Information</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <div class="form-group">
              <label>Brand</label>
              <select class="form-control" name="brand_id">
                  <?php recursive_option($brand, $_POST["brand_id"]) ?>
              </select>
            </div>
            <div class="form-group">
              <label>Category</label>
              <select class="form-control" name="category_id">
                  <?php
                  if (isset($_POST["category"])) {
                      recursive_option($recursive, $_POST["category"]);
                  } else {
                      recursive_option($recursive, $old['category_id']);
                  }
                  ?>
              </select>
            </div>
            <div class="form-group">
              <label>Product name</label>
              <input type="text" name="name" class="form-control" placeholder="Please input product name" <?php
              // If input new data -> show new data. If not, show old data
              if (isset($_POST["name"])) {
                  echo 'value="' . $_POST["name"] . '"';
              } else {
                  echo 'value="' . $product["name"] . '"';
              }
              ?>>
            </div>
            <div class="form-group">
              <label>Price</label>
              <input type="text" name="price" class="form-control" placeholder="Please input product price" <?php
              if (isset($_POST["price"])) {
                  echo 'value="' . $_POST["price"] . '"';
              } else {
                  echo 'value="' . $product["price"] . '"';
              }
              ?>>
            </div>
            <div class="form-group">
              <label>Sale price</label>
              <input type="text" name="sale_price" class="form-control" placeholder="Please input Sale price" <?php
              if (isset($_POST["sale_price"])) {
                  echo 'value="' . $_POST["sale_price"] . '"';
              } else {
                  echo 'value="' . $product["sale_price"] . '"';
              }
              ?>>
            </div>
            <div class="form-group">
              <label>Introduction</label>
              <textarea class="form-control" name="intro"><?php
                  if (isset($_POST["intro"])) {
                      echo $_POST["intro"];
                  } else {
                      echo $product["intro"];
                  }
                  ?></textarea>
              <script>
                CKEDITOR.replace('intro', {
                  filebrowserBrowseUrl: 'http://localhost/ApTech/eProject/ap-admin/public/assets/plugins/ckfinder/ckfinder.html',
                  filebrowserUploadUrl: 'http://localhost/ApTech/eProject/ap-admin/public/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                })
              </script>
            </div>
            <div class="form-group">
              <label>Content</label>
              <textarea class="form-control" name="content"><?php
                  if (isset($_POST["content"])) {
                      echo $_POST["content"];
                  } else {
                      echo $product["content"];
                  }
                  ?></textarea>
              <script>
                CKEDITOR.replace('content', {
                  filebrowserBrowseUrl: 'http://localhost/ApTech/eProject/ap-admin/public/assets/plugins/ckfinder/ckfinder.html',
                  filebrowserUploadUrl: 'http://localhost/ApTech/eProject/ap-admin/public/assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                })
              </script>
            </div>
            <!-- Image -->
            <div class="form-group">
              <label>Current image</label>
              <img src="../public/assets/image-product/<?php echo $product["image"] ?>" onerror="imgError(this);"
                   width="100px" class="d-block" />
            </div>
            <div class="form-group">
              <label>Image</label>
              <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="customFile">
                <label class="custom-file-label" for="customFile">Choose file</label>
              </div>
            </div>
            <div class="form-group">
              <label>Status</label>
              <select class="form-control" name="status">
                <option value="1" <?php
                if ($product["status"] == 1) {
                    echo "selected";
                }
                ?>>On
                </option>
                <option value="0" <?php
                if ($product["status"] == 0) {
                    echo "selected";
                }
                ?>>Off
                </option>
              </select>
            </div>
            <div class="form-group">
              <label>Featured</label>
              <select class="form-control" name="featured">
                <option value="0" <?php
                if ($product["featured"] == 0) {
                    echo "selected";
                }
                ?>>Off
                </option>
                <option value="1" <?php
                if ($product["featured"] == 1) {
                    echo "selected";
                }
                ?>>On
                </option>
              </select>
            </div>
          </div>
          <div class="card-footer">
            <button type="submit" name="edit" class="btn btn-info">Edit</button>
            <button type="reset" class="btn btn-default float-right">Delete</button>
          </div>
        </div>
      </div>
  </form>
<?php } ?>
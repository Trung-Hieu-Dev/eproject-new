<?php 
if (!isset($_GET["id"])) {
    header("location:index.php?module=product&action=index");
    exit();
} else {
    $id = $_GET["id"];
    
    if (!check_product_id ($conn,$id)) {
        header("location:index.php?module=product&action=index");
        exit();
    }

    $product = get_product ($conn,$id);

    if (file_exists('../public/assets/image-product/'.$product["image"])) {
        // Delete image firstly before delete data
        unlink('../public/assets/image-product/'.$product["image"]);
    }
    
    delete_product ($conn,$id);

    header("location:index.php?module=product&action=index");
    exit();
}
?>
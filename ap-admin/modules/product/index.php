<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Product list</h3>
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Price</th>
                            <th>Sale price</th>
                            <th>Status</th>
                            <th>Feature</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $products = get_all_product($conn);
                        foreach ($products as $product) {
                        ?>
                            <tr>
                                <!-- Show image -->
                                <td>
                                    <img onerror="imgError(this);" src="<?php echo $product["image"] ?>" width="80px" />
                                </td>
                                <td><?php echo $product["name"] ?></td>
                                <td><?php echo $product["cname"] ?></td>
                                <td><?php echo $product["brand_name"] ?></td>
                                <td>USD <?php echo number_format($product["price"], 2, ".", ",") ?></td>
                                <td>USD <?php echo number_format($product["sale_price"], 2, ".", ",") ?></td>
                                <td>
                                    <?php
                                    if ($product["status"] == 1) {
                                        echo '<div class="badge badge-primary">On</div>';
                                    } else {
                                        echo '<div class="badge badge-secondary">Off</div>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($product["featured"] == 1) {
                                        echo '<div class="badge badge-primary">On</div>';
                                    } else {
                                        echo '<div class="badge badge-secondary">Off</div>';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a class="confirmDelete badge badge-danger" href="index.php?module=product&action=delete&id=<?php echo $product["id"] ?>">Delete</a> |
                                    <a class="badge badge-warning" href="index.php?module=product&action=edit&id=<?php echo $product["id"] ?>">Edit</a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Price</th>
                            <th>Sale price</th>
                            <th>Status</th>
                            <th>Feature</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
<?php
// Show content need to edit, need to check id-> list
if (!isset($_GET["id"])) {
    header("location:index.php?module=banner&action=index");
    exit();
} else {
    $errors = array();
    $id = $_GET["id"];
    $all_banner = list_banner($conn);

    if (!check_banner_id($conn, $id)) {
        header("location:index.php?module=banner&action=index");
        exit();
    }
    // Get banner out thru ID
    $old_banner = get_banner($conn, $id);

    if (isset($_POST["edit"])) {

        if (empty($_POST["name"])) {
            $errors[] = "Please input banner name";
        }

        if (empty($_POST["slug"])) {
            $errors[] = "Please enter your slug";
        }

        if (empty($errors)) {

            $data = array(
                'name' => $_POST["name"],
                'slug' => $_POST["slug"],
                'status' => (isset($_POST["status"]) && ($_POST["status"] == 'on')) ? 1 : 0,
                'id' => $id
            );
            // Check banner exist before adding, if it si ok, then edit
            if (check_banner_exist($conn, $data, true)) {
                // If no new image, use old image. If insert new image, check image in correct format or not then store in database
                if (!empty($_FILES["image"]["name"])) {
                    if (!checkExt($_FILES["image"]["name"])) {
                        $errors[] = "It is not an image file";
                    } else {
                        $file = change_name_file($_FILES["image"]["name"]);
                        // New image
                        $data["image"] = $file;

                        move_uploaded_file($_FILES["image"]["tmp_name"], './public/assets/brand_image/' . $file);
                        edit_banner($conn, $data);

                        header("location:index.php?module=banner");
                        exit();
                    }
                } else {
                    // Old image
                    $data["image"] = $old_banner["image"];
                    edit_banner($conn, $data);

                    header("location:index.php?module=banner");
                    exit();
                }
            } else {
                $errors[] = "banner name is existed";
            }
        }
    }
?>

    <?php if (!empty($errors)) { ?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Error!</h5>
            <ul>
                <?php foreach ($errors as $error) { ?>
                    <li><?php echo $error ?></li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>


    <form method="POST" action="" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Input Your Information</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->

                    <div class="card-body">
                        <div class="form-group">
                            <label>Banner Name</label>
                            <input type="text" class="form-control convert_slug" name="name" data-slug="slug" placeholder="Please enter your category name" <?php keep_value_input('name', $old_banner["name"]); ?>>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Slug</label>
                            <input type="text" class="form-control" name="slug" placeholder="Please enter your slug" <?php keep_value_input('name', $old_banner["slug"]); ?>>
                        </div>

                        <!-- Image -->
                        <div class="form-group">
                            <label>Current image</label>
                            <img src="./public/assets/brand_image/<?php echo $old_banner["image"] ?>" onerror="imgError(this);" width="100px" class="d-block" />
                        </div>
                        <div class="form-group">
                            <label>Image</label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        <input type="checkbox" class="bootstrap_switch" name="status" data-bootstrap-switch data-off-color="danger" data-on-color="success" <?php
                                                                                                                                                            if ($old_banner["status"] == 1) {
                                                                                                                                                                echo 'checked';
                                                                                                                                                            }
                                                                                                                                                            ?>>
                    </div>
                    <div class="card-footer">
                        <button type="submit" name="edit" class="btn btn-primary">Submit</button>
                    </div>

                </div>
            </div>
        </div>
    </form>

    <!-- /.card -->
<?php } ?>
<?php

$data = list_banner($conn);

?>

<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">List Banners</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Num</th>
              <th>Name</th>
              <th>Slug</th>
              <th>Status</th>
              <th>Banner Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $stt = 0;
            foreach ($data as $item) {
              $stt++;
            ?>
              <tr>
                <td><?php echo $stt ?></td>
                <td><?php echo $item["name"] ?></td>
                <td><?php echo $item["slug"] ?></td>
                <td>
                  <?php
                  if ($item["status"] == 1) {
                    echo '<div class="badge badge-primary">Acitve</div>';
                  } else {
                    echo '<div class="badge badge-info">Disable</div>';
                  }
                  ?>
                </td>
                <td>
                  <img onerror="imgError(this);" src="./public/assets/brand_image/<?php echo $item["image"] ?>" width="200px" />
                </td>
                <td>
                  <a class="confirmDelete badge badge-danger" href="index.php?module=banner&action=delete&id=<?php echo $item["id"] ?>">Delete</a> |
                  <a class="badge badge-warning" href="index.php?module=banner&action=edit&id=<?php echo $item["id"] ?>">Edit</a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th>Num</th>
              <th>Name</th>
              <th>Slug</th>
              <th>Status</th>
              <th>Banner </th>
              <th>Action</th>

            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>
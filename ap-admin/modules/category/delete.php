<?php

if (isset($_GET["id"])) {
    $id = $_GET["id"];

    settype($id, 'int');

    if($id != 0) {
        if(check_category_child($conn, $id)) {
            delete_category($conn, $id);
            header("location:index.php?module=category");
            exit();
        } else {
            echo '<script>
                alert("Can not delete parent category")
                window.location.href="index.php?module=category"
            </script>';
            exit();
        }

    } else {
        echo 'Can not delete';
    }
}

?>
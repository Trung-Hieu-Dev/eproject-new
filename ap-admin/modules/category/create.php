<?php
$errors = array();

$data = list_category($conn);

if (isset($_POST["submit"])) {
    if (empty($_POST["name"])) {
        $errors[] = "Please enter your category name";
    }

    if (empty($_POST["slug"])) {
        $errors[] = "Please enter your slug";
    }

    if (empty($errors)) {

        $data = array(
            'name' => $_POST["name"],
            'slug' => $_POST["slug"],
            'parent' => $_POST["parent"],
            'status' => (isset($_POST["status"]) && ($_POST["status"] == 'on')) ? 1 : 0
        );

        create_category($conn, $data, $errors[]);
        header("location:index.php?module=category&action=index");
        exit();

    }
}
?>

<!-- Alert start -->
<?php if (!empty($errors)) { ?>

  <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
      <?php
      foreach ($errors as $error) {
          echo '<li>' . $error . '</li>';
      }
      ?>
  </div>

<?php } ?>
<!-- /.alert -->


<div class="row">
  <div class="col-lg-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Input Your Information</h3>
      </div>
      <!-- /.card-header -->
      <!-- form start -->
      <form action="" method="POST">
        <div class="card-body">
          <div class="form-group">
            <label>Select Menu</label>
            <select name="parent" class="form-control">
              <option value="0">--- Root ---</option>
                <?php recursive_option($data, $_POST["parent"]); ?>
            </select>
          </div>
          <div class="form-group">
            <label>Category Name</label>
            <input type="text" class="form-control convert_slug" name="name" data-slug="slug"
                   placeholder="Please enter your category name" <?php keep_value_input('name'); ?>>
          </div>
          <div class="form-group">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" placeholder="Please enter your slug">
          </div>

          <input type="checkbox" class="bootstrap_switch" name="status" checked data-bootstrap-switch
                 data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="submit" name="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
    <!-- /.card -->
  </div>
</div>
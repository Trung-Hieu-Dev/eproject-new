<?php
$data = list_category($conn);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">List Category</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="dataTable" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Num</th>
                            <th>name</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Parent</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php   
                            $stt = 0;
                            foreach ($data as $key => $item) {
                            $stt++; 
                        ?>
                            
                            <tr>
                                <td><?php echo $stt ?></td>
                                <td><?php echo $item["name"] ?></td>
                                <td><?php echo $item["slug"] ?></td>
                                <td>
                                    <?php
                                    if ($item["status"] == 1) {
                                        echo '<div class="badge badge-primary">Acitve</div>';
                                    } else {
                                        echo '<div class="badge badge-info">Disable</div>';
                                    }
                                    ?>
                                </td>
                                
                                <td>
                                    <?php  
                                        for ($i=0 ; $i < sizeof($data) ; $i++ ) { 
                                            if ($data[$i]['id'] == $item['parent']) {
                                                echo $data[$i]['name'];
                                                break;
                                            }
                                        }
                                    ?>
                                </td>

                                <td>
                                    <a class="confirmDelete badge badge-danger" href="index.php?module=category&action=delete&id=<?php echo $item["id"] ?>">Delete</a> |
                                    <a class="badge badge-warning" href="index.php?module=category&action=edit&id=<?php echo $item["id"] ?>">Edit</a>
                                </td>
                            </tr>
                        <?php }  ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Num</th>
                            <th>name</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Parent</th>
                            <th>Action</th>
            </div>
        </div>
        </tr>
        </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
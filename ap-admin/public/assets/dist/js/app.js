$('a.remove-item').confirm({
	    title: 'Encountered an error!',
	    content: 'Remove product from the basket.',
	    type: 'red',	
	    buttons: {
        tryAgain: {
            text: 'yes, remove',
            btnClass: 'btn-red',
            action: function(){
	            	value = this.$target.attr('data-id');
	            	location.href ="cart.php?action=empty&sku="  + value;
            }
        },
        close: function () {
        	//event
        }	    	

	    }
});	

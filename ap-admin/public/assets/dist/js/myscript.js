$(document).ready(function () {
  $('input[class=\'bootstrap_switch\']').bootstrapSwitch()

  $('.convert_slug').keyup(function () {
    var value = $(this).val()
    var inputSlug = $(this).data('slug')
    var convertSlug = ChangeToSlug(value)
    $('input[name=\'' + inputSlug + '\']').val(convertSlug)
  })

  $('#dataTable').DataTable({
    'responsive': true, 'lengthChange': false, 'autoWidth': false,
    'buttons': ['copy', 'csv', 'excel', 'pdf', 'print', 'colvis'],
  }).buttons().container().appendTo('#dataTable_wrapper .col-md-6:eq(0)')

  // btn confirm delete
  $('.confirmDelete').click(function () {
    if (confirm('Are you sure you want to delete?')) {
      return true
    }
    return false
  })

  // btn confirm logout
  $('.confirmLogout').click(function () {
    if (confirm('Are you sure you want to log out?')) {
      return true
    }
    return false
  })

  $('.confirmCancel').click(function () {
    if (confirm('Are you suer you want to cancel?')) {
      return true
    }
    return false
  })

  // toggle edit address detail
  $('#my-card-widget').CardWidget('toggle')

  //Bootstrap Duallistbox
  $('.duallistbox').bootstrapDualListbox()

})

function ChangeToSlug (title) {

  //Đổi chữ hoa thành chữ thường
  slug = title.toLowerCase()

  //Đổi ký tự có dấu thành không dấu
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a')
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e')
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i')
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o')
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u')
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y')
  slug = slug.replace(/đ/gi, 'd')
  //Xóa các ký tự đặt biệt
  slug = slug.replace(
    /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi,
    '')
  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, '-')
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, '-')
  slug = slug.replace(/\-\-\-\-/gi, '-')
  slug = slug.replace(/\-\-\-/gi, '-')
  slug = slug.replace(/\-\-/gi, '-')
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = '@' + slug + '@'
  slug = slug.replace(/\@\-|\-\@|\@/gi, '')

  //In slug ra textbox có id “slug”
  return slug
}

// CREATE USER ADDRESS
// initial ajax
const ajax = new XMLHttpRequest()
const baseUrl = 'model/address/'
let urlToQuery = `${baseUrl}/getProvince.php`
const async = true
const optionProvince = document.getElementById('province')
const optionDistrict = document.getElementById('district')
const optionWard = document.getElementById('ward')

// receive response
const appendAddressOption = (type, code) => {
  ajax.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      const response = JSON.parse(this.responseText)
      type.innerHTML += response.map((val) => {
        switch (code) {
          case 'maqh':
            return `<option id="${code}" value="${val.maqh}">${val.name}</option>`
          case 'xaid':
            return `<option id="${code}" value="${val.xaid}">${val.name}</option>`
          default:
            return `<option value="${val.matp}">${val.name}</option>`
        }
      }).join('')
    }
  }
  ajax.open('GET', urlToQuery, async)
  ajax.send()
}

// initial province options
if (optionProvince.childElementCount <= 1) {
  appendAddressOption(optionProvince, 'matp')
}

// remove option element
const removeOptionElement = (parent) => {
  const len = parent.options.length - 1
  for (let i = len; i >= 0; i--) {
    if (parent.options[i].id !== '') {
      parent.remove(i)
    }
  }
  parent.selectedIndex = 0
}

// onchange to show relate district and ward
const addressCodeSelect = (type) => {
  if (type === 'province') {
    removeOptionElement(optionDistrict)
    if (optionWard.childElementCount > 1) {
      removeOptionElement(optionWard)
    }
    const matp = optionProvince.value
    urlToQuery = `${baseUrl}/getDistrictBaseOnProvince.php?q=${matp}`
    appendAddressOption(optionDistrict, 'maqh')
  } else if (type === 'district') {
    removeOptionElement(optionWard)
    const maqh = optionDistrict.value
    urlToQuery = `${baseUrl}/getWardBaseOnDistrict.php?q=${maqh}`
    appendAddressOption(optionWard, 'xaid')
  }
}

const onInputEmailHandler = (input) => {
  ajax.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      const response = JSON.parse(this.responseText)
      if (response.length > 0) {
        const {
          user_id,
          email,
          full_name,
          province_city,
          district,
          ward,
          address_detail,
          phone,
        } = response[0]
        const customerDetail = {
          user_id,
          email,
          full_name,
          province_city,
          district,
          ward,
          address_detail,
          phone,
        }
        document.cookie = `customer-detail=${JSON.stringify(customerDetail)}`
        location.reload()
      }
      if (response.length <= 0) {
        document.cookie = `customer-detail=Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;`
        location.reload()
      }
    }
  }
  ajax.open('GET', `model/user/get-user-by-email.php?email=${input.value}`,
    async)
  ajax.send()
}

// list all product
function delay (fn, ms) {
  let timer = 0
  return function (...args) {
    clearTimeout(timer)
    timer = setTimeout(fn.bind(this, ...args), ms || 0)
  }
}

const searchInputHandler = document.getElementById('search-product')
const searchProduct = document.getElementById('search-product-result')
const searchProductResult = document.getElementById(
  'search-product-result-item')
const productQuantity = document.getElementById('product-quantity')
const priceTotal = document.getElementById('price-total')

window.addEventListener('click', function (e) {
    if (searchInputHandler.contains(e.target)) {
      searchInputHandler.addEventListener('keyup', delay((e) => {
        const searchKeyword = e.target.value
        if (searchKeyword) {
          searchProduct.classList.add('search-result-active')
        } else {
          searchProduct.classList.remove('search-result-active')
        }
        ajax.onreadystatechange = function () {
          if (this.readyState === 4 && this.status === 200) {
            const response = JSON.parse(this.responseText)
            const ulList = document.querySelectorAll(
              '#search-product-result-item li')
            for (let i = 0; li = ulList[i]; i++) {
              li.parentNode.removeChild(li)
            }
            if (response.length > 0) {
              response.map((val) => {
                searchProductResult.innerHTML += '<li value="' + val.id +
                  '" onclick="onSelectProductHandler(\'' + val.id + '\',\'' + val.name + '\',\'' + val.sale_price + '\')">[' +
                  (val.brand_name).replaceAll(
                    ' ', '') + '-' + val.id + ']. ' + val.name + '</li>'
              })
            } else {
              searchProductResult.innerHTML = `<li value="" style="pointer-events: none;">No result to select</li>`
            }
          }
        }
        ajax.open('GET',
          `model/search-product-by-name.php?q=${searchKeyword}`)
        ajax.send()
      }))
    } else {
      searchProduct.classList.remove('search-result-active')
    }
  },
)

function getCookie (cname) {
  let name = cname + '='
  let decodedCookie = decodeURIComponent(document.cookie)
  let ca = decodedCookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}

const addZeroes = (num) => {
  let value = Number(num)
  const res = num.split('.')
  if (res.length === 1 || (res[1].length < 3)) {
    value = value.toFixed(2)
  }
  return value
}

let pricePerProduct = 0

const onSelectProductHandler = (productId, productName, productPrice) => {
  const getCartData = getCookie('customer-cart')
  if (getCartData) {
    const currentCart = JSON.parse(getCartData)
    const newCart = {
      id: productId,
      quantity: 1,
    }
    currentCart.push(newCart)
    document.cookie = `customer-cart=${JSON.stringify(currentCart)}`
  }
  if (!getCartData) {
    document.cookie = `customer-cart=${JSON.stringify(
      [{ id: productId, quantity: 1 }])}`
  }
  pricePerProduct = productPrice
  searchInputHandler.value = productName
  productQuantity.value = 1
  const total = 1 * productPrice
  priceTotal.value = `$${addZeroes(`${total}`)}`
}

const onChangeQuantityHandler = (quantity) => {
  if (quantity.value <= 1) {
    productQuantity.value = 1
  }
  const total = quantity.value * pricePerProduct
  priceTotal.value = `$${addZeroes(`${total}`)}`
}

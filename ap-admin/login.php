<?php
session_start();
ob_start();

if (isset($_SESSION["user_profile"])) {
    header("location:index.php?module=home");
    exit();
}

include '../core/config.php';
include '../core/connect.php';
include './model/auth.php';

$errors = array();

if (isset($_POST["submit"])) {
    if (empty($_POST["email"])) {
        $errors[] = "Please enter a email address.";
    } else {
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Invalid email address.";
        }
    }

    if (empty($_POST["password"])) {
        $errors[] = "Please enter your password";
    }

    if (empty($errors)) {
        $data = array(
            "email" => $_POST["email"],
            "password" => $_POST["password"]
        );
        $response = user_login($conn, $data);
        if ($response) {
            $_SESSION["user_profile"] = $response[0];
            header("location:index.php?module=user");
            exit();
        } else {
            $errors[] = 'Email or password incorrect';
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aptech - eProject | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="./public/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="./public/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="./public/assets/dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <img src="./public/assets/images/logo.png" alt="webpage-logo">
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to Safira Admin Page</p>

      <!-- Alert start -->
        <?php if (!empty($errors)) { ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">×
            </button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
              <?php
              foreach ($errors as $error) {
                  echo '<li>' . $error . '</li>';
              }
              ?>
          </div>
        <?php } ?>
      <!-- /.alert -->

      <form action="" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email"
                 placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password"
                 placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col-4">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="./public/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="./public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="./public/assets/dist/js/adminlte.min.js"></script>
</body>
</html>

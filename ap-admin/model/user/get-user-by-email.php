<?php
include __DIR__ . '/../../../core/config.php';
include __DIR__ . '/../../../core/connect.php';

$customer_email = $_GET["email"];

$stmt = $conn->prepare("SELECT * FROM user LEFT JOIN user_info ON user.id = user_info.user_id WHERE user.email = :email AND user.level = 2 AND user.status = 1");
$stmt->bindValue(":email", $customer_email);
$stmt->execute();
$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($response);
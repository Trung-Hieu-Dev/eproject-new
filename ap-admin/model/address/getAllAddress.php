<?php
function get_a_province($conn, $matp)
{
    $stmt = $conn->prepare("SELECT * FROM province_city WHERE matp = :matp");
    $stmt->bindParam(":matp", $matp, PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function get_a_district($conn, $maqh)
{
    $stmt = $conn->prepare("SELECT * FROM district WHERE maqh = :maqh");
    $stmt->bindParam(":maqh", $maqh, PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function get_a_ward($conn, $xaid)
{
    $stmt = $conn->prepare("SELECT * FROM ward WHERE xaid = :xaid");
    $stmt->bindParam(":xaid", $xaid, PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


<?php
include __DIR__ . '/../../../core/config.php';
include __DIR__ . '/../../../core/connect.php';

$q = intval($_GET['q']);

$stmt = $conn->prepare("SELECT * FROM district WHERE matp = :matp");
$stmt->bindParam(":matp", $q, PDO::PARAM_INT);
$stmt->execute();
$district_response = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($district_response);
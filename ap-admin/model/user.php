<?php

use Ramsey\Uuid\Nonstandard\Uuid;

require_once __DIR__ . '/../../vendor/autoload.php';

// check user exist
function check_user_exists($conn, $data)
{
    $stmt = $conn->prepare("SELECT * FROM user WHERE email = :email");
    $stmt->bindParam(':email', $data["email"], PDO::PARAM_STR);
    $stmt->execute();
    $row = $stmt->rowCount();
    if ($row > 0) {
        return true;
    }
}

//create user
function create_user($conn, $data, &$success)
{
    // generate uuid -> primary key
    $uuid = Uuid::uuid4();
    $password_hash = password_hash($data["password"], PASSWORD_DEFAULT);

    $stmt = $conn->prepare("INSERT INTO user (id, email, password, level, status) VALUES (:id, :email, :password, :level, :status)");
    $stmt->bindValue(':id', $uuid);
    $stmt->bindParam(':email', $data["email"], PDO::PARAM_STR);
    $stmt->bindValue(':password', $password_hash);
    $stmt->bindParam(':level', $data["level"], PDO::PARAM_INT);
    $stmt->bindParam(':status', $data["status"], PDO::PARAM_INT);
    $stmt->execute();
    insert_user_info($conn, $data, $uuid);
    $success = true;
}

// update user info
function insert_user_info($conn, $data, $id)
{
    $stmt = $conn->prepare("INSERT INTO user_info (full_name, phone, province_city, district, ward, address_detail, avatar_img, user_id) VALUES (:full_name, :phone, :province_city, :district, :ward, :address_detail, :avatar_img, :user_id)");
    if (!is_null($data["avatar_img"])) {
        $stmt->bindParam(':avatar_img', $data["avatar_img"], PDO::PARAM_STR);
    } else {
        $stmt->bindValue(':avatar_img', null);
    }
    $stmt->bindParam(':full_name', $data["full_name"], PDO::PARAM_STR);
    $stmt->bindParam(':phone', $data["phone"], PDO::PARAM_STR);
    $stmt->bindParam(':province_city', $data["province_city"], PDO::PARAM_STR);
    $stmt->bindParam(':district', $data["district"], PDO::PARAM_STR);
    $stmt->bindParam(':ward', $data["ward"], PDO::PARAM_STR);
    $stmt->bindParam(':address_detail', $data["address_detail"], PDO::PARAM_STR);
    $stmt->bindParam(':user_id', $id, PDO::PARAM_STR);
    $stmt->execute();
}

// get all user info
function get_all_user_info($conn)
{
    $stmt = $conn->prepare("SELECT * FROM user LEFT JOIN user_info ON user.id = user_info.user_id");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// get user by id
function get_user_info($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM user LEFT JOIN user_info ON user.id = user_info.user_id WHERE user.id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// update user
function update_user($conn, $data, &$success)
{
    if ($data["password"]) {
        $password_hash = password_hash($data["password"], PASSWORD_DEFAULT);
        $stmt = $conn->prepare("UPDATE user SET password = :password, level = :level, status = :status WHERE id = :id");
        $stmt->bindValue(":password", $password_hash);
    } else {
        $stmt = $conn->prepare("UPDATE user SET level = :level, status = :status WHERE id = :id");
    }
    $stmt->bindParam(":level", $data["level"], PDO::PARAM_INT);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["user_id"], PDO::PARAM_STR);
    $stmt->execute();
    update_user_profile($conn, $data);
    $success = true;
}

// update user profile
function update_user_profile($conn, $data)
{
    if (!is_null($data["province_city"]) && !is_null($data["district"]) && !is_null($data["ward"]) && !is_null($data["address_detail"])) {
        $stmt = $conn->prepare("UPDATE user_info SET full_name = :full_name, ward = :ward, province_city = :province_city, district = :district, address_detail = :address_detail, phone = :phone, avatar_img = :avatar_img WHERE user_id = :user_id");
        $stmt->bindParam(':province_city', $data["province_city"], PDO::PARAM_STR);
        $stmt->bindParam(':district', $data["district"], PDO::PARAM_STR);
        $stmt->bindParam(':ward', $data["ward"], PDO::PARAM_STR);
        $stmt->bindParam(':address_detail', $data["address_detail"], PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("UPDATE user_info SET full_name = :full_name, phone = :phone, avatar_img = :avatar_img WHERE user_id = :user_id");
    }
    $stmt->bindParam(':full_name', $data["full_name"], PDO::PARAM_STR);
    $stmt->bindParam(':phone', $data["phone"], PDO::PARAM_STR);
    $stmt->bindParam(':avatar_img', $data["avatar_img"], PDO::PARAM_STR);
    $stmt->bindParam(':user_id', $data["user_id"], PDO::PARAM_STR);
    $stmt->execute();
}

// delete user
function delete_user($conn, $id)
{
    $stmt = $conn->prepare("DELETE user, user_info FROM user LEFT JOIN user_info ON user.id = user_info.user_id WHERE user.id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
}
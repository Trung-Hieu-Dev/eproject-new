<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

// search by name
$keyword = "'" . $_GET["q"] . "'";

$stmt = $conn->prepare("SELECT p.*, c.name as cname, c.slug as cslug, b.name as brand_name FROM category as c, product as p, brand as b WHERE MATCH(p.name) AGAINST(:keyword) AND c.id = p.category_id AND b.id = p.brand_id");

//

$stmt->bindValue(":keyword", $keyword);
$stmt->execute();
$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($response);


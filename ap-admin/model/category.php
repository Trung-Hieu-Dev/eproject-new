<?php
// Create Category
function create_category($conn, $data, &$errors) {
    $stmt_check = $conn->prepare("SELECT * FROM category WHERE name = :name");
    $stmt_check->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt_check->execute();
    $row = $stmt_check->rowCount();

    if ($row == 0) {
        $stmt = $conn->prepare("INSERT INTO category (name, slug, parent, status) VALUES (:name, :slug, :parent, :status)");
        $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
        $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
        $stmt->bindParam(":parent", $data["parent"], PDO::PARAM_INT);
        $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
        $stmt->execute();

    } else {
        $errors = "This name is already existed";
    }
}

// Get all category
function list_category($conn, $edit = false, $id = null) {
    if($edit) {
        $stmt = $conn->prepare("SELECT * FROM category WHERE id != :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    } else {
        $stmt = $conn->prepare("SELECT * FROM category");
    }

    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Delete category
function delete_category($conn, $id) {
    $stmt = $conn->prepare("DELETE FROM category WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
}

// Check Category Child
function check_category_child($conn, $id) {
    $stmt = $conn->prepare("SELECT * FROM category WHERE parent = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $count = $stmt->rowCount();
    if($count > 0) {
        return false;
    }
    return true;
}

// Get old category
function get_old_category($conn, $id) {
    $stmt = $conn->prepare("SELECT * FROM category WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

// Edit Category
function edit_category($conn, $data, &$errors)
{

    $stmt_check = $conn->prepare("SELECT * FROM category WHERE name = :name AND id != :id");
    $stmt_check->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt_check->bindParam(":id", $data["id"], PDO::PARAM_STR);
    $stmt_check->execute();
    $row = $stmt_check->rowCount();

    if ($row == 0) {
        $stmt = $conn->prepare("UPDATE category SET name = :name, slug = :slug, parent = :parent, status = :status WHERE id = :id");
        $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
        $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
        $stmt->bindParam(":parent", $data["parent"], PDO::PARAM_INT);
        $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
        $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
        $stmt->execute();
        header("location:index.php?module=category&action=index");
        exit();
        

    } else {
        $errors = "This name is already existed";
    }
}
?>
<?php
function user_login($conn, $data)
{
    $stmt = $conn->prepare("SELECT u.id, u.email, u.password, u.level, u.status, uf.full_name, uf.avatar_img FROM user u LEFT JOIN user_info uf ON u.id = uf.user_id WHERE u.email = :email AND u.status != 0");
    $stmt->bindParam(":email", $data["email"], PDO::PARAM_STR);
    $stmt->execute();
    $response = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($response) {
        if (in_array($response[0]["level"], [0, 1])) {
            if (password_verify($data["password"], $response[0]["password"])) {
                unset($response[0]["password"]);
                return $response;
            }
        }
    }
    return false;
}
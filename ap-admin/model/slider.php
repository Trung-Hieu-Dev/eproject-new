<?php

// Create slider
function create_slider($conn, $data)
{
    
    $stmt = $conn->prepare("INSERT INTO slider (name, slug, image, status) VALUES (:name, :slug, :image, :status)");
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":image", $data["image"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;

}

// Get all sliders
function list_slider($conn)
{ 
    $stmt = $conn->prepare("SELECT * FROM slider");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


// Get old slider
function get_slider($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM slider WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

// Check slider exist
function check_slider_exist ($conn, $data, $edit = false) {
    if(!$edit) {
        $stmt_check = $conn->prepare("SELECT * FROM slider WHERE name = :name");

    } else {
        $stmt_check = $conn->prepare("SELECT * FROM slider WHERE name = :name AND id != :id");
        $stmt_check->bindParam(":id", $data["id"], PDO::PARAM_STR);
    }
    $stmt_check->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt_check->execute();
    $row = $stmt_check->rowCount();

    if ($row > 0) {
        return false;
    }
    
    return true;

}

function check_slider_id($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM slider WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0) {
        return true;
    }

    return false;
}

// Edit slider
function edit_slider($conn, $data)
{    
    $stmt = $conn->prepare("UPDATE slider SET name = :name, slug = :slug, image = :image, status = :status WHERE id = :id");
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":image", $data["image"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;
           
}

// Delete slider
function delete_slider($conn, $id)
{
    $stmt = $conn->prepare("DELETE FROM slider WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;
}

?>
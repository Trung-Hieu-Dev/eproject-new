<!-- Get all brand -->
<?php
function get_all_brand($conn, $edit = false, $id = null)
{
    if ($edit) {
        $stmt = $conn->prepare("SELECT * FROM brand WHERE id != :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("SELECT * FROM brand");
    }

    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

// Get all data of category 
function get_all_category($conn, $edit = false, $id = null)
{
    if ($edit) {
        $stmt = $conn->prepare("SELECT * FROM category WHERE id != :id");
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("SELECT * FROM category");
    }

    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

// Create product
function create_product($conn, $product)
{
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $date = date('Y-m-d H:i:s', time());

    $stmt = $conn->prepare("INSERT INTO `product`(`name`, `price`,`sale_price`, `intro`, `content`, `image`, `status`, `featured`, `category_id`, `brand_id`, `created_at`) VALUES (:name, :price, :sale_price, :intro, :content, :image, :status, :featured, :category_id, :brand_id, :created_at)");
    $stmt->bindParam(':name', $product["name"], PDO::PARAM_STR);
    $stmt->bindParam(':price', $product["price"], PDO::PARAM_INT);
    $stmt->bindParam(':sale_price', $product["sale_price"], PDO::PARAM_INT);
    $stmt->bindParam(':intro', $product["intro"], PDO::PARAM_STR);
    $stmt->bindParam(':content', $product["content"], PDO::PARAM_STR);
    $stmt->bindParam(':image', $product["image"], PDO::PARAM_STR);
    $stmt->bindParam(':status', $product["status"], PDO::PARAM_INT);
    $stmt->bindParam(':featured', $product["featured"], PDO::PARAM_INT);
    $stmt->bindParam(':category_id', $product["category_id"], PDO::PARAM_INT);
    $stmt->bindParam(':brand_id', $product["brand_id"], PDO::PARAM_INT);
    $stmt->bindValue(':created_at', $date);
    $stmt->execute();
    return $stmt;
}

// Edit product
function edit_product($conn, $product)
{
    $stmt = $conn->prepare("UPDATE product SET name= :name,price= :price,sale_price= :sale_price,intro= :intro,content= :content,image= :image,status= :status,featured= :featured,category_id= :category_id,brand_id= :brand_id WHERE id = :id");
    $stmt->bindParam(':name', $product["name"], PDO::PARAM_STR);
    $stmt->bindParam(':price', $product["price"], PDO::PARAM_INT);
    $stmt->bindParam(':sale_price', $product["sale_price"], PDO::PARAM_INT);
    $stmt->bindParam(':intro', $product["intro"], PDO::PARAM_STR);
    $stmt->bindParam(':content', $product["content"], PDO::PARAM_STR);
    $stmt->bindParam(':image', $product["image"], PDO::PARAM_STR);
    $stmt->bindParam(':status', $product["status"], PDO::PARAM_INT);
    $stmt->bindParam(':featured', $product["featured"], PDO::PARAM_INT);
    $stmt->bindParam(':category_id', $product["category_id"], PDO::PARAM_INT);
    $stmt->bindParam(':brand_id', $product["brand_id"], PDO::PARAM_INT);
    $stmt->bindParam(':id', $product["id"], PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;
}

// Check product exist
function check_product_exist($conn, $data, $edit = false)
{
    if (!$edit) {
        $stmt = $conn->prepare("SELECT name FROM product WHERE name = :name");
    } else {
        $stmt = $conn->prepare("SELECT name FROM product WHERE name = :name AND id != :id");
        $stmt->bindParam(':id', $data["id"], PDO::PARAM_STR);
    }

    $stmt->bindParam(':name', $data["name"], PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0) {
        return false;
    }

    return true;
}

// Get product data
function get_all_product($conn)
{
    // Connect table to get data, prodct get all data p*, category: get name only
    $stmt = $conn->prepare("SELECT p.*,c.name as cname, b.name as brand_name FROM product as p, category as c, brand as b WHERE p.category_id = c.id AND b.id = p.brand_id ORDER BY p.id DESC");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

// Edit: get product to edit by ID
function get_product($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

function check_product_id($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0) {
        return true;
    }

    return false;
}

function delete_product($conn, $id)
{
    $stmt = $conn->prepare("DELETE FROM product WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();

    return $stmt;
}

function data_recursive($conn)
{
    $stmt = $conn->prepare("SELECT * FROM category");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function get_one_product($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

?>


<?php

// Create Brand
function create_brand($conn, $data)
{
    
    $stmt = $conn->prepare("INSERT INTO brand (name, slug, image, status) VALUES (:name, :slug, :image, :status)");
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":image", $data["image"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;

}

// Get all brands
function list_brand($conn)
{ 
    $stmt = $conn->prepare("SELECT * FROM brand");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


// Get old Brand
function get_brand($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM brand WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

// Check brand exist
function check_brand_exist ($conn, $data, $edit = false) {
    if(!$edit) {
        $stmt_check = $conn->prepare("SELECT * FROM brand WHERE name = :name");

    } else {
        $stmt_check = $conn->prepare("SELECT * FROM brand WHERE name = :name AND id != :id");
        $stmt_check->bindParam(":id", $data["id"], PDO::PARAM_STR);
    }
    $stmt_check->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt_check->execute();
    $row = $stmt_check->rowCount();

    if ($row > 0) {
        return false;
    }
    
    return true;

}

function check_brand_id($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM brand WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0) {
        return true;
    }

    return false;
}

// Edit Brand
function edit_brand($conn, $data)
{    
    $stmt = $conn->prepare("UPDATE brand SET name = :name, slug = :slug, image = :image, status = :status WHERE id = :id");
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":image", $data["image"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;
           
}

// Delete brand
function delete_brand($conn, $id)
{
    $stmt = $conn->prepare("DELETE FROM brand WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt;
}


?>
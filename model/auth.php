<?php
function customer_login($conn, $data)
{
    $stmt = $conn->prepare("SELECT u.id, u.email, u.password, u.level, u.status, uf.full_name, uf.province_city, uf.district, uf.ward, uf.address_detail, uf.phone, uf.avatar_img FROM user u LEFT JOIN user_info uf ON u.id = uf.user_id WHERE u.email = :email AND u.status != 0");
    $stmt->bindParam(":email", $data["email"], PDO::PARAM_STR);
    $stmt->execute();
    $response = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($response) {
        if ($response[0]["level"] == 2) {
            if (password_verify($data["password"], $response[0]["password"])) {
                unset($response[0]["password"]);
                return $response;
            }
        }
    }
    return false;
}
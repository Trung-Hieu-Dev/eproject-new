<?php

// Get all brands
function list_brand($conn)
{
    $stmt = $conn->prepare("SELECT * FROM brand");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

?>
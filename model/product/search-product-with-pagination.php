<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

$keyword = "'" . $_GET["q"] . "'";
$page = $_GET["page"] ?? 1;
$records_per_page = 5;
$page_start_position = $records_per_page * ($page - 1);
$query_limit = "LIMIT $records_per_page OFFSET $page_start_position";

if (!isset($_GET["brand"]) || $_GET["brand"] == 'null' || $_GET["brand"] == '') {
    $stmt = $conn->prepare("
SELECT p.*,
       c.name                       as cname,
       c.slug                       as cslug,
       (SELECT count(*)
        FROM category as c,
             product as p
        WHERE MATCH(p.name) AGAINST(:keyword)
AND c.id = p.category_id) as total_records
FROM category as c,
     product as p
WHERE MATCH(p.name) AGAINST(:keyword)
AND c.id = p.category_id $query_limit");
} else {
    $brand_name = $_GET["brand"];
    $stmt = $conn->prepare("
SELECT p.*,
       b.name                                                            as bname,
       b.slug                                                            as bslug,
       (SELECT count(*)
        FROM brand as b,
             product as p
        WHERE MATCH(p.name) AGAINST(:keyword)
          AND b.id = p.brand_id
          AND p.brand_id = (SELECT id FROM brand WHERE slug = '$brand_name')) as total_records
FROM brand as b,
     product as p
WHERE MATCH(p.name) AGAINST(:keyword)
  AND b.id = p.brand_id
  AND p.brand_id = (SELECT id FROM brand WHERE slug = '$brand_name') $query_limit");
}
$stmt->bindValue(":keyword", $keyword);
$stmt->execute();
$response = $stmt->fetchAll(PDO::FETCH_ASSOC);

if ($response == []) {
    echo json_encode($response);
}

if($response != []) {
    $data = array(
        "totalRecord" => $response[0]["total_records"],
        "data" => $response
    );
    echo json_encode($data);
}

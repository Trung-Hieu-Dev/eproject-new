<?php
// Get product data
function get_all_product($conn)
{
    // Connect table to get data, product get all data p*, category: get name only
    $stmt = $conn->prepare("SELECT p.*,c.name as cname, b.name as brand_name FROM product as p, category as c, brand as b WHERE p.category_id = c.id AND b.id = p.brand_id ORDER BY p.id DESC LIMIT 5");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


// Get all product data
function all_product($conn)
{
    // Connect table to get data, prodct get all data p*, category: get name only
    $stmt = $conn->prepare("SELECT * FROM product LIMIT 5");
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}


// get product by ID
function get_product($conn, $id)
{
    $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_STR);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

// Get product with product id
function get_one_product($conn, $id)
{
    // Connect table to get data, prodct get all data p*, category: get name only
    $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug FROM product as p, category as c WHERE p.category_id = c.id ORDER BY p.id = :id DESC");

    $stmt->bindParam(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

?>
<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

// search by name
$keyword = "'" . $_GET["q"] . "'";

if (!isset($_GET["brand"]) || $_GET["brand"] == 'null' || $_GET["brand"] == '') {
    $stmt = $conn->prepare("SELECT p.*, c.name as cname, c.slug as cslug FROM category as c, product as p WHERE MATCH(p.name) AGAINST(:keyword) AND c.id = p.category_id");
} else {
    $brand_name = $_GET["brand"];
    $stmt = $conn->prepare("SELECT p.*, b.name as bname, b.slug as bslug
        FROM brand as b, product as p
        WHERE MATCH(p.name) AGAINST(:keyword)
        AND b.id = p.brand_id AND p.brand_id = (SELECT id FROM brand WHERE slug = '$brand_name')");
}
$stmt->bindValue(":keyword", $keyword);
$stmt->execute();
$response = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($response);

?>

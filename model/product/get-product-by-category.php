<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

$slug = $_GET["slug"];
$start = $_GET["start"];
$end = $_GET["end"];
$sort_by = $_GET["sort"] ?? '';
$order_by_sql = "";

switch ($sort_by) {
    case 'low':
        $order_by_sql = "p.sale_price ASC";
        break;
    case 'high':
        $order_by_sql = "p.sale_price DESC";
        break;
    case 'name_desc':
        $order_by_sql = "p.name ASC";
        break;
    default:
        $order_by_sql = "created_at DESC";
}

$stmt = $conn->prepare("SELECT p.*, c.name as cname, c.slug as cslug
        FROM category as c, product as p
        WHERE c.id = p.category_id
        AND c.slug = :slug
        ORDER BY $order_by_sql
        LIMIT :start, :end");
$stmt->bindParam(':slug', $slug, PDO::PARAM_STR);
$stmt->bindParam(':start', $start, PDO::PARAM_INT);
$stmt->bindParam(':end', $end, PDO::PARAM_INT);
$stmt->execute();
$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($response);

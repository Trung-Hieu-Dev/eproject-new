<?php
function get_a_province($conn, $matp, $type = 'text')
{
    if ($type == 'text') {
        $stmt = $conn->prepare("SELECT * FROM province_city WHERE matp = :matp");
        $stmt->bindParam(":matp", $matp, PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("SELECT * FROM province_city");
    }
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function get_a_district($conn, $maqh, $matp = null, $type = 'text')
{
    if ($type == 'text') {
        $stmt = $conn->prepare("SELECT * FROM district WHERE maqh = :maqh");
        $stmt->bindParam(":maqh", $maqh, PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("SELECT * FROM district WHERE matp = :matp");
        $stmt->bindParam(":matp", $matp, PDO::PARAM_STR);
    }
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function get_a_ward($conn, $xaid, $maqh = null, $type = 'text')
{
    if ($type == 'text') {
        $stmt = $conn->prepare("SELECT * FROM ward WHERE xaid = :xaid");
        $stmt->bindParam(":xaid", $xaid, PDO::PARAM_STR);
    } else {
        $stmt = $conn->prepare("SELECT * FROM ward WHERE maqh = :maqh");
        $stmt->bindParam(":maqh", $maqh, PDO::PARAM_STR);
    }
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

$stmt = $conn->prepare("SELECT * FROM province_city");
$stmt->execute();
$province_response = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($province_response);
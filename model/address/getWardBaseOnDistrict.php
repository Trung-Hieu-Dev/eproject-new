<?php
include __DIR__ . '/../../core/config.php';
include __DIR__ . '/../../core/connect.php';

$q = intval($_GET['q']);

$stmt = $conn->prepare("SELECT * FROM ward WHERE maqh = :maqh");
$stmt->bindParam(":maqh", $q, PDO::PARAM_INT);
$stmt->execute();
$ward_reponse = $stmt->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($ward_reponse);
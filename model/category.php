<?php

// Get all parent category
function all_category($conn)
{
    
    $stmt = $conn->prepare("SELECT * FROM category WHERE parent = 0");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Get child category
function category($conn)
{

    $stmt = $conn->prepare("SELECT * FROM category WHERE parent = 3 AND (id = 5 OR id = 6 OR id = 7)");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

?>
<?php

use Ramsey\Uuid\Nonstandard\Uuid;

require_once __DIR__ . '/../vendor/autoload.php';

$checkout_id = Uuid::uuid4();

function checkout($conn, $data, $create_account, &$errors, &$success)
{
    $user_id = null;
    if ($create_account == 'on') {
        if (check_user_exists($conn, $data["email"])) {
            $errors = 'Email is already exists';
        } else {
            create_user($conn, $data, $user_id,);
        }
    }

    if (!is_null($user_id)) {
        
        add_to_cart($conn, $data, $user_id);
        unset($_SESSION['cart']);
        header("location:index.php?p=bill&id=". $GLOBALS['checkout_id']);
        exit;
    } else {

        add_to_cart($conn, $data);
        unset($_SESSION['cart']);
        header("location:index.php?p=bill&id=" . $GLOBALS['checkout_id']);
        exit;
    }
    
}

//create user
function create_user($conn, $data, &$user_id)
{
    // generate uuid -> primary key
    $uuid = Uuid::uuid4();
    $password_hash = password_hash($data["password"], PASSWORD_DEFAULT);

    $stmt = $conn->prepare("INSERT INTO user (id, email, password, level, status) VALUES (:id, :email, :password, :level, :status)");
    $stmt->bindValue(':id', $uuid);
    $stmt->bindParam(':email', $data["email"], PDO::PARAM_STR);
    $stmt->bindValue(':password', $password_hash);
    $stmt->bindValue(':level', 2);
    $stmt->bindValue(':status', 1);
    $stmt->execute();
    insert_user_info($conn, $data, $uuid);
    $user_id = $uuid;
}

// update user info
function insert_user_info($conn, $data, $id)
{
    $stmt = $conn->prepare("INSERT INTO user_info (full_name, phone, province_city, district, ward, address_detail, user_id) VALUES (:full_name, :phone, :province_city, :district, :ward, :address_detail, :user_id)");
    $stmt->bindParam(':full_name', $data["full_name"], PDO::PARAM_STR);
    $stmt->bindParam(':phone', $data["phone"], PDO::PARAM_STR);
    $stmt->bindParam(':province_city', $data["province_city"], PDO::PARAM_STR);
    $stmt->bindParam(':district', $data["district"], PDO::PARAM_STR);
    $stmt->bindParam(':ward', $data["ward"], PDO::PARAM_STR);
    $stmt->bindParam(':address_detail', $data["address_detail"], PDO::PARAM_STR);
    $stmt->bindParam(':user_id', $id, PDO::PARAM_STR);
    $stmt->execute();
}


// add data to cart table
function add_to_cart($conn, $data, $user_id = null )
{
    $uuid = $GLOBALS['checkout_id'];
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $date = date('Y-m-d H:i:s', time());

    if (isset($data["id"]) || !is_null($user_id)) {
        $stmt = $conn->prepare("INSERT INTO cart (id, full_name, email, phone, province_city, district, ward, address_detail, status, user_id, created_at) VALUES (:id, :full_name, :email, :phone, :province_city, :district, :ward, :address_detail, :status, :user_id, :created_at)");
        if (isset($data["id"])) {
            $stmt->bindParam('user_id', $data["id"], PDO::PARAM_STR);
        }
        if (!is_null($user_id)) {
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_STR);
        }
    } else {
        $stmt = $conn->prepare("INSERT INTO cart (id, full_name, email, phone, province_city, district, ward, address_detail, status, created_at) VALUES (:id, :full_name, :email, :phone, :province_city, :district, :ward,:address_detail, :status, :created_at)");
    }
    $stmt->bindValue(':id', $uuid);
    $stmt->bindParam(':full_name', $data["full_name"], PDO::PARAM_STR);
    $stmt->bindParam(':email', $data["email"], PDO::PARAM_STR);
    $stmt->bindParam(':phone', $data["phone"], PDO::PARAM_STR);
    $stmt->bindParam(':province_city', $data["province_city"], PDO::PARAM_STR);
    $stmt->bindParam(':district', $data["district"], PDO::PARAM_STR);
    $stmt->bindParam(':ward', $data["ward"], PDO::PARAM_STR);
    $stmt->bindParam(':address_detail', $data["address_detail"], PDO::PARAM_STR);
    $stmt->bindValue(':status', 2);
    $stmt->bindValue(':created_at', $date);
    $stmt->execute();
    add_cart_detail($conn, $data, $uuid);


}



// add data to cart detail
function add_cart_detail($conn, $data, $cart_id)
{
    $sql = array();
    foreach ($data["cart"] as $product) {
        $sql[] = '(' . $product["product_price"] . ', ' . $product["quantity"] . ', ' . $product["total_price"] . ', "' . $cart_id . '", ' . $product["product_id"] . ')';
    }

    $stmt = $conn->prepare("INSERT INTO cart_detail (product_price, quantity, total_price, cart_id, product_id) VALUES " . implode(',', $sql));
    $stmt->execute();
}


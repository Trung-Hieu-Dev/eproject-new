<?php
include __DIR__ . '/../model/address/getAllAddress.php';

// Keep value
function keep_value_input($name, $value = null)
{
    if (isset($_POST[$name])) {
        echo 'value = "' . $_POST[$name] . '"';
    }

    if (!is_null($value)) {
        echo 'value = "' . $value . '"';
    }
}

// Recursive
function recursive_option($data, $seleted, $parent = 0, $str = "")
{
    foreach ($data as $key => $value) {
        if ($value["parent"] == $parent) {

            $seleted_option = "";
            if ($value["id"] == $seleted) {
                $seleted_option = "selected";
            }

            echo '<option value="' . $value["id"] . '" ' . $seleted_option . ' >' . $str . $value["name"] . '</option>';
            unset($data[$key]);

            recursive_option($data, $seleted, $value["id"], $str . "---| ");
        }
    }
}

// Check images
function check_ext($file_name)
{
    $extensions = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');
    if (in_array($file_name, $extensions)) {
        return true;
    } else {
        return false;
    }
}

// Change file name
function change_name_file($file_name)
{
    $file_name = trim($file_name);
    $file_name = mb_strtolower($file_name);
    $file_name = preg_replace('!\s+!', ' ', $file_name);
    $file_name = str_replace(" ", "-", $file_name);
    $file_name = time() . "-" . $file_name;
    return $file_name;
}

// ***** Product
// CSS sidebar use class menu-open & active
function active_menu($module, $action, $type = 'active')
{
    if (isset($_GET['module']) && $_GET['module'] == $module) {
        if (isset($_GET['action']) && in_array($_GET['action'], $action)) {
            if ($type == 'active') {
                echo 'active';
            } else {
                echo 'menu-open';
            }
        }
    }
}

// Product-check it is image file or not (Ext: extention)
function checkExt($filename)
{
    $pos = strrpos($filename, ".") + 1;
    $ext = substr($filename, $pos);
    if ($ext != "png" && $ext != "jpeg" && $ext != "jpg" && $ext != "gif") {
        return false;
    } else {
        return true;
    }
}

// display address
function display_address($conn, $address_detail, $ward, $district, $province, $type = 'text')
{
    $response = get_address_name($conn, $ward, $district, $province);
    if ($response) {
        $ward_name = $response["ward"][0];
        $district_name = $response["district"][0];
        $province_name = $response["province"][0];
    }
    if ($type == 'text' && $ward_name && $district_name && $province_name) {
        echo "$address_detail" . ', ' . $ward_name["name"] . ', ' . $district_name["name"] . ', ' . $province_name["name"];
    }
}

// get name address name
function get_address_name($conn, $ward, $district, $province): array
{
    return $data = array(
        "ward" => get_a_ward($conn, $ward),
        "district" => get_a_district($conn, $district),
        "province" => get_a_province($conn, $province)
    );
}


// keep value option
function keep_value_option($selected, $option_value, $old = null)
{
    if (isset($_POST[$selected])) {
        echo ($_POST[$selected] == $option_value) ? 'selected' : '';
    }

    if (!is_null($old)) {
        echo ($old == $option_value) ? 'selected' : '';
    }
}

// keep address option value
function keep_province_address_option($conn, $province_code)
{
    $response = get_a_province($conn, $province_code, 'option');
    foreach ($response as $item) {
        $selected = $item['matp'] == $province_code ? 'selected' : '';
        echo '<option id="matp" value="' . $item["matp"] . '"' . $selected . '>' . $item["name"] . '</option>';
    }
}

function keep_district_address_option($conn, $province_code, $district_code)
{
    $response = get_a_district($conn, $district_code, $province_code, 'option');
    foreach ($response as $item) {
        $selected = $item['maqh'] == $district_code ? 'selected' : '';
        echo '<option id="maqh" value="' . $item["maqh"] . '"' . $selected . '>' . $item["name"] . '</option>';
    }
}

function keep_ward_address_option($conn, $district_code, $ward_code)
{
    $response = get_a_ward($conn, $ward_code, $district_code, 'option');
    foreach ($response as $item) {
        $selected = $item['xaid'] == $ward_code ? 'selected' : '';
        echo '<option id="xaid" value="' . $item["xaid"] . '"' . $selected . '>' . $item["name"] . '</option>';
    }
}

?>

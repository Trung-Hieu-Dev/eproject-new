<?php
if (isset($_GET['p']) && $_GET['p'] == 'brand') {


    if (isset($_GET['slug'])) {
        $slug = $_GET['slug'];
        $itemOnPerPage = 5;

        function count_all_product_by_brand_id($conn, $slug)
        {
            $stmt = $conn->prepare("SELECT p.*, b.name as bname, b.slug as bslug 
                FROM brand as b, product as p
                WHERE b.id = p.brand_id
                AND b.slug = :slug");
            $stmt->bindParam(':slug', $slug, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->rowCount();
        }

        $totalItem = count_all_product_by_brand_id($conn, $slug);

        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }

        $start = ($page - 1) * $itemOnPerPage;
        $end = $itemOnPerPage;
        $totalPage = ceil($totalItem / $itemOnPerPage);
    } else {
        header('location:index.php');
        exit;
    }
} else {
    header('location:index.php');
    exit;
}
?>

<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb_content">
          <h3>Brands</h3>
          <ul>
            <li><a href="index.php">home</a></li>
            <li>Brand</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--breadcrumbs area end-->

<!--shop  area start-->
<div class="shop_area shop_reverse mt-70 mb-70">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-12">
        <!--sidebar widget start-->
          <?php include './pages/brand/blocks/brand-sidebar.php' ?>
        <!--sidebar widget end-->
      </div>
      <div class="col-lg-9 col-md-12">
        <!--shop wrapper start-->

        <!-- shop start -->
          <?php include './pages/brand/blocks/brand-shop.php' ?>
        <!-- shop end -->

      </div>
    </div>
  </div>
</div>
<!--shop  area end-->
<!--shop toolbar start-->
<div class="shop_toolbar_wrapper">
  <div class="shop_toolbar_btn">
    <button data-role="grid_3" type="button" class=" btn-grid-3" data-toggle="tooltip" title="3"></button>
    <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip" title="4"></button>
    <button data-role="grid_list" type="button" class="active btn-list" data-toggle="tooltip" title="List"></button>
  </div>
  <div class=" niceselect_option">
    <select class="address-dropdown" id="product_short" onchange="onSortProduct()">
      <!--        <option selected value="1">Sort by average rating</option>-->
      <!--        <option value="2">Sort by popularity</option>-->
      <option value="new" selected>Sort by: Newness</option>
      <option value="low">Sort by price: Low to high</option>
      <option value="high">Sort by price: High to low</option>
      <option value="name_desc">Product Name: A-Z</option>
    </select>
  </div>
  <div class="page_amount" style="text-align: center">
    <p>Showing <?php echo $start ?> – <?php echo $start + $itemOnPerPage ?> of <?php echo $totalItem ?> results</p>
  </div>
</div>
<!--shop toolbar end-->
<div id="brand-shop-container">
  <div id="toolbar-container" class="row shop_wrapper grid_list"></div>
  <div id="toolbar-pagination" class="shop_toolbar t_bottom">
    <div class="pagination">
      <ul>
          <?php for ($i = 1; $i <= $totalPage; $i++) { ?>
            <li>
              <a href="index.php?p=brand&slug=<?php echo $slug ?>&page=<?php echo $i ?>"
                 class="links"><?php echo $i ?></a>
            </li>
          <?php } ?>
      </ul>
    </div>
  </div>
  <p id="product-not-found" style="text-align: center">No product to display.</p>
</div>


<script>
  const slug = '<?php echo $slug; ?>'
  const start = <?php echo $start; ?>;
  const end = <?php echo $end; ?>;

  const ajax = new XMLHttpRequest()
  const async = true
  const brandBaseUrl = 'model/brand'
  const allProductByBrandUrl = `${brandBaseUrl}/get-product-by-brand.php?slug=${slug}&start=${start}&end=${end}`

  const rangeFrom = document.getElementById('range-from')
  const rangeTo = document.getElementById('range-to')

  const parentElement = document.getElementById('brand-shop-container')
  const toolbarElement = document.getElementById('toolbar-container')
  const paginationElement = document.getElementById('toolbar-pagination')
  const notFound = document.getElementById('product-not-found')
  const productSort = document.getElementById('product_short')

  let rangeFromValue = null
  let rangeToValue = null

  const addZeroes = (num) => {
    let value = Number(num)
    const res = num.split('.')
    if (res.length === 1 || (res[1].length < 3)) {
      value = value.toFixed(2)
    }
    return value
  }

  const mapDataToDisplay = (data) => {
    toolbarElement.innerHTML = data.map((val) => {
      return `
        <div class="col-12">
          <div class="single_product">
            <div class="product_thumb">
              <a class="primary_img" href="index.php?p=product-detail&id=${val.id}"><img
                    style="height: 100%;" src="${val.image}" alt="${val.id}-${val.name}"></a>
              <div class="label_product">
                <span class="label_sale">Sale</span>
                <span class="label_new">New</span>
              </div>
              
            </div>
            <div class="product_content grid_content">
              <h4 class="product_name"><a
                    href="index.php?p=product-detail&id=${val.id}">${val.name}</a></h4>
              <p>
                <a href="index.php?p=brand&slug=${val.bslug}&page=<?php echo $page ?>">${val.bname}</a>
              </p>
              <div class="price_box">
                <span class="current_price">USD ${addZeroes(`${val.sale_price}`)}</span>
                <span class="old_price">USD ${addZeroes(`${val.price}`)}</span>
              </div>
            </div>
            <div class="product_content list_content">
              <h4 class="product_name"><a
                    href="index.php?p=product-detail&id=${val.id}">${val.name}</a></h4>
              <p>
                <a href="index.php?p=brand&slug=${val.bslug}&page=<?php echo $page ?>">${val.bname}</a>
              </p>
              <div class="price_box">
                <span class="current_price">USD ${addZeroes(`${val.sale_price}`)}</span>
                <span class="old_price">USD ${addZeroes(`${val.price}`)}</span>
              </div>
              <div class="product_desc">${val.intro}</div>
              
            </div>
          </div>
        </div>
      `
    }).join('')
  }

  const fetchBrandFilterData = (url) => {
    ajax.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        const response = JSON.parse(this.responseText)
        if (response.length <= 0) {
          paginationElement.style.display = 'none'
          notFound.style.display = 'block'
        } else {
          notFound.style.display = 'none'
          paginationElement.style.display = 'flex'
        }
        mapDataToDisplay(response)
      }
    }
    ajax.open('GET', url, async)
    ajax.send()
  }

  fetchBrandFilterData(allProductByBrandUrl)

  const onFilterBtnClick = () => {
    productSort.options[0].selected = true
    rangeFromValue = rangeFrom.value.replace('$', '')
    rangeToValue = rangeTo.value.replace('$', '')
    const filterByPriceUrl = `${brandBaseUrl}/get-brand-filter-by-price.php?slug=${slug}&start=${start}&end=${end}&from=${rangeFromValue}&to=${rangeToValue}`
    fetchBrandFilterData(filterByPriceUrl)
  }

  const onSortProduct = () => {
    const sortByValue = productSort.value
    let sortByUrl = ''
    if (!rangeFromValue && !rangeToValue) {
      sortByUrl = `${brandBaseUrl}/get-product-by-brand.php?slug=${slug}&start=${start}&end=${end}&sort=${sortByValue}`
    } else {
      sortByUrl = `${brandBaseUrl}/get-brand-filter-by-price.php?slug=${slug}&start=${start}&end=${end}&from=${rangeFromValue}&to=${rangeToValue}&sort=${sortByValue}`
    }
    fetchBrandFilterData(sortByUrl)
  }
</script>

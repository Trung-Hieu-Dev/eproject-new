<!--breadcrumbs area start-->
<?php include './pages/wishlist/blocks/breadcrumb.php' ?>
<!--breadcrumbs area end-->


<!--wishlist area start -->
<div class="wishlist_area mt-70">
    <div class="container">
        <form action="#">
            <div class="row">
                <div class="col-12">
                    <div class="table_desc wishlist">
                        <div class="cart_page">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product_remove">Delete</th>
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Stock Status</th>
                                        <th class="product_total">Add To Cart</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($_SESSION['wishlist']) && !empty($_SESSION['wishlist'])) {
                                        $ids = array_keys($_SESSION["wishlist"]);
                                        $inQuery = implode(',', array_fill(0, count($ids), '?')); // IN (?, ?, ?)
                                        $stmt = $conn->prepare("SELECT id, name, image, sale_price FROM product WHERE id IN(" . $inQuery . ")");
                                        foreach ($ids as $k => $id) {
                                            $stmt->bindValue(($k + 1), $id);
                                        }
                                        $stmt->execute();
                                        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($products as $product) {


                                    ?>
                                            <tr>
                                                <td class="product_remove "><a href="" class="removeItemWishlist" data-id="<?php echo $product['id'] ?>">X</a></td>
                                                <td class="product_thumb"><a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><img src="<?php echo $product['image'] ?>" alt=""></a></td>
                                                <td class="product_name product_id"><a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><?php echo $product['name'] ?></a></td>
                                                <td class="product-price">USD <?php echo number_format($product["sale_price"], 2, ".", ",") ?></td>
                                                <td class="product_quantity"><label>In stock</label></td>
                                                <td class="product_total"><a href="#" class="adToCart" data-id="<?php echo $product['id'] ?>">Add To Cart</a></td>
                                            </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

        </form>
        <div class="row">
            <div class="col-12">
                <div class="wishlist_share">
                    <h4>Share on:</h4>
                    <ul>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
<!--wishlist area end -->
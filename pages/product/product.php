<?php
if (isset($_GET['p']) && $_GET['p'] == 'shop-list') {
  if (isset($_GET['slug'])) {
    $slug = $_GET['slug'];
    $itemOnPerPage = 5;

    function count_all_product_by_category_id($conn, $slug)
    {
      $stmt = $conn->prepare("SELECT p.*, c.name as cname, c.slug as cslug 
                FROM category as c, product as p
                WHERE c.id = p.category_id
                AND c.slug = :slug");
      $stmt->bindParam(':slug', $slug, PDO::PARAM_STR);
      $stmt->execute();
      return $stmt->rowCount();
    }

    $totalItem = count_all_product_by_category_id($conn, $slug);

    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    } else {
      $page = 1;
    }

    $start = ($page - 1) * $itemOnPerPage;
    $end = $itemOnPerPage;
    $totalPage = ceil($totalItem / $itemOnPerPage);
  } else {
    header('location:index.php');
    exit;
  }
} else {
  header('location:index.php');
  exit;
}

?>


<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb_content">
          <h3>Shop</h3>
          <ul>
            <li><a href="index.php">home</a></li>
            <li><a href=""><?php echo $slug ?></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--breadcrumbs area end-->

<!--shop  area start-->
<div class="shop_area shop_reverse mt-70 mb-70">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-12">
        <!--sidebar widget start-->
        <?php include './pages/product/blocks/sidebar.php' ?>
        <!--sidebar widget end-->
      </div>
      <div class="col-lg-9 col-md-12">
        <!--shop wrapper start-->

        <!-- shop start -->
        <?php include './pages/product/blocks/shop.php' ?>
        <!-- shop end -->

      </div>
    </div>
  </div>
</div>
<!--shop  area end-->
<?php

// session_start();
?>
<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="index.php">home</a></li>
                        <li>product details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->

<!--product details start-->
<?php include './pages/product/blocks/detail-head.php' ?>
<!--product details end-->

<!--product info start-->
<!-- <?php include './pages/product/blocks/info.php' ?> -->
<!--product info end-->

<!--product area start-->
<?php include './pages/product/blocks/related-product.php' ?>
<!--product area end-->

<!--product area start-->
<?php include './pages/product/blocks/upsale.php' ?>
<!--product area end-->
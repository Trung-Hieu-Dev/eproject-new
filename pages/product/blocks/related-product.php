<?php

function get_product_by_shoes_category($conn)
{
    $stmt = $conn->prepare("SELECT p.*, c.name as cname, c.slug as cslug
        FROM category as c, product as p
        WHERE c.id = p.category_id
        AND p.category_id = 5
        AND c.slug = 'shoes'
        ORDER BY p.id DESC
        LIMIT 10");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$product = get_product_by_shoes_category($conn);


?>



<section class="product_area related_products">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2>Related Products </h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product_carousel product_column5 owl-carousel">
                    <?php foreach ($product as $item) { ?>
                        <article class="single_product">
                            <figure>
                                <div class="product_thumb">
                                    <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img style="height: 100%;" src="<?php echo $item["image"] ?>" alt=""></a>
                                    <div class="label_product">
                                        <span class="label_sale">Sale</span>
                                        <span class="label_new">New</span>
                                    </div>
                                    <div class="action_links">
                                        <ul>
                                            <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                            <!-- <li class="quick_button"><a href="#" data-tippy="quick view" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true" data-bs-toggle="modal" data-bs-target="#modal_box"> <span class="lnr lnr-magnifier"></span></a></li> -->
                                            <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                            <!-- <li class="compare"><a href="#" data-tippy="Add to Compare" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-sync"></span></a></li> -->
                                        </ul>
                                    </div>
                                </div>
                                <figcaption class="product_content">
                                    <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item["name"] ?></a></h4>
                                    <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item["cname"] ?></a></p>
                                    <div class="price_box">
                                        <span class="current_price">USD <?php echo number_format($item["sale_price"], 2, ".", ",") ?></span>
                                        <span class="old_price">USD <?php echo number_format($item["price"], 2, ".", ",") ?></span>
                                    </div>
                                </figcaption>
                            </figure>
                        </article>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php

if (isset($_GET['p']) && $_GET['p'] == 'product-detail') {
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $product = get_one_product($conn, $id);
    } else {
        header('location:index.php');
        exit();
    }
} else {
    header('location:index.php');
    exit();
}

?>

<div class="product_details mt-70 mb-70">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product-details-tab">
                    <div id="img-1" class="zoomWrapper single-zoom">
                        <a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>">
                            <img style="height: 100%;" id="zoom1" src="<?php echo $product["image"] ?>" data-zoom-image="<?php echo $product["image"] ?>" alt="big-1">
                        </a>
                    </div>

                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product_d_right">
                    <form action="#" method="POST">

                        <h1><a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><?php echo $product['name'] ?></a></h1>

                        <div class=" product_ratting">
                            <ul>
                                <li><a href="#"><i class="icon-star"></i></a></li>
                                <li><a href="#"><i class="icon-star"></i></a></li>
                                <li><a href="#"><i class="icon-star"></i></a></li>
                                <li><a href="#"><i class="icon-star"></i></a></li>
                                <li><a href="#"><i class="icon-star"></i></a></li>
                                <li class="review"><a href="#"> (customer review ) </a></li>
                            </ul>

                        </div>
                        <div class="price_box">
                            <span class="current_price">USD <?php echo $product['sale_price'] ?></span>
                            <span class="old_price">USD <?php echo $product['price'] ?></span>

                        </div>
                        <div class="product_desc">
                            <p><?php echo $product['intro'] ?></p>
                        </div>

                        <div class="product_variant quantity">
                            <label>quantity</label>
                            <input min="1" max="100" <?php
                                                        $fl = 0;
                                                        if (isset($_SESSION['cart'])) {
                                                            foreach ($_SESSION['cart'] as $id => $quantity) {
                                                                if ($id == $_GET['id']) {
                                                                    $fl = 1;
                                                                    break;
                                                                }
                                                            }

                                                            if ($fl == 1) {
                                                                echo 'value="' . $quantity . '"';
                                                            } else {
                                                                echo 'value="1"';
                                                            }
                                                        } else {
                                                            echo 'value="1"';
                                                        }

                                                        ?> type="number" name="quantity" data-id="<?php echo $product['id'] ?>">
                            <button type="submit" name="adToCart" class="button" id="adToCart" onClick="window.location.reload();">add to cart</button>

                        </div>
                        <div class=" product_d_action">
                            <ul>
                                <li><a class="add-to-wishlist" data-id="<?php echo $product['id'] ?>" href="#" title="Add to wishlist" onClick="window.location.reload();">+ Add to Wishlist</a></li>
                                <!-- <li><a href="#" title="Add to wishlist">+ Compare</a></li> -->
                            </ul>
                        </div>
                        <div class="product_meta">
                            <span>Category: <a href="index.php?p=shop-list&slug=<?php echo $product['cslug'] ?>"><?php echo $product['cname'] ?></a></span>
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>

<!-- Info start -->
<div class="product_d_info mb-65">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="product_d_inner">
                    <div class="product_info_button">
                        <ul class="nav" role="tablist" id="nav-tab">
                            <li>
                                <a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a>
                            </li>

                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="info" role="tabpanel">
                            <div class="product_info_content">
                                <?php echo $product['content'] ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Info end -->
<div class="about_gallery_section">
    <div class="container">
        <div class="about_gallery_container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <article class="single_gallery_section">
                        <figure>
                            <div class="gallery_thumb">
                                <img src="./ap-admin/public/assets/brand_image/1632847292-about2.jpg" alt="">
                            </div>
                            <figcaption class="about_gallery_content">
                                <h3>Trendy women’s clothing</h3>
                                <p> Online shopping for women at Myntra is a mood-elevating experience. Striped dresses and T-shirts represent the classic spirit of nautical fashion. Our salwar-kameez sets, kurtas and Patiala suits make comfortable options for regular wear.</p>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <div class="col-lg-4 col-md-4">
                    <article class="single_gallery_section">
                        <figure>
                            <div class="gallery_thumb">
                                <img src="./ap-admin/public/assets/brand_image/1632847313-about3.jpg" alt="">
                            </div>
                            <figcaption class="about_gallery_content">
                                <h3>Stylish accessories</h3>
                                <p>Myntra is the best online shopping sites for classy accessories that perfectly complement your outfits. Our collection offers impressive options. Online shopping for kids at Myntra is a complete joy.</p>
                            </figcaption>
                        </figure>
                    </article>
                </div>
                <div class="col-lg-4 col-md-4">
                    <article class="single_gallery_section">
                        <figure>
                            <div class="gallery_thumb">
                                <img src="./ap-admin/public/assets/brand_image/1632847325-about4.jpg" alt="">
                            </div>
                            <figcaption class="about_gallery_content">
                                <h3>Beauty begins here</h3>
                                <p>You can also refresh, rejuvenate and reveal beautiful skin with personal care, beauty and grooming products from Myntra. Reducing the effect of aging and offer the ideal cleansing experience</p>
                            </figcaption>
                        </figure>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
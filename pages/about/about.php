<?php

$stmt = $conn->prepare("SELECT * FROM slider WHERE slug = 'about-store'");
$stmt->execute();
$slider = $stmt->fetch(PDO::FETCH_ASSOC);


?>

<!--breadcrumbs area start-->
<?php include './pages/about/blocks/breadcrumb.php' ?>
<!--breadcrumbs area end-->

<!--about section area -->
<section class="about_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <figure>
                    <div class="about_thumb">
                        <img src="./ap-admin/public/assets/brand_image/<?php echo $slider["image"] ?>" alt="">
                    </div>
                    <figcaption class="about_content">
                        <h1>ONLINE SHOPPING MADE EASY AT MYNTRA</h1>
                        <p>If you would like to experience the best of online shopping for men, women and kids in India, you are at the right place. Myntra is the ultimate destination for fashion and lifestyle, being host to a wide array of merchandise including clothing, footwear, accessories, jewellery, personal care products and more. It is time to redefine your style statement with our treasure-trove of trendy items. Our online store brings you the latest in designer products straight out of fashion houses. You can shop online at Myntra from the comfort of your home and get your favourites delivered right to your doorstep.</p>

                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</section>
<!--about section end-->

<!--chose us area start-->
<?php include './pages/about/blocks/choose.php' ?>

<!--chose us area end-->

<!--services img area-->
<?php include './pages/about/blocks/image.php' ?>
<!--services img end-->

<!--testimonial area start-->
<?php include './pages/about/blocks/testimol.php' ?>
<!--testimonial area end-->
<?php

if (!empty($_SESSION['cart'])) {
    header('loaction:index.php');
    exit;
}

if (isset($_GET['id'])) {
    $id_bill = $_GET['id'];

    // Connect table to get data, prodct get all data p*, category: get name only
    function get_id_bill($conn, $id)
    {
        $stmt = $conn->prepare("SELECT c.*, sum(cd.total_price) as cdtotal_price 
        FROM cart as c, cart_detail as cd 
        WHERE cd.cart_id = c.id
        AND c.id = '$id'");
        // $stmt->bindParam(":c.id", $id_bill, PDO::PARAM_STR);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    $bill = get_id_bill($conn, $id_bill);
}

?>

<!-- my account start  -->
<section class="main_content_area">
    <div class="container">
        <div class="account_dashboard">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <!-- Tab panes -->
                    <div class="tab-content dashboard_content">
                        <div class="tab-pane fade show active" id="orders">
                            <h3>Orders Success !</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Order No</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $bill['id'] ?></td>
                                            <td><?php echo $bill['created_at'] ?></td>
                                            <td>USD <?php echo $bill['cdtotal_price'] ?></td>
                                            <td><span class="success"><?php echo $bill['full_name'] ?></span></td>
                                            <td><?php echo $bill['email'] ?></a></td>
                                            <td><?php echo $bill['phone'] ?></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- my account end   -->
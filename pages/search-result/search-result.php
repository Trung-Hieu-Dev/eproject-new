<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb_content">
          <h3>Shop</h3>
          <ul>
            <li><a href="index.php">home</a></li>
            <li><a href="">Search</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--breadcrumbs area end-->

<!--shop  area start-->
<div class="shop_area shop_reverse mt-70 mb-70">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-12">
        <!--sidebar widget start-->
          <?php include './pages/search-result/blocks/sidebar.php' ?>
        <!--sidebar widget end-->
      </div>
      <div class="col-lg-9 col-md-12">
        <!--shop wrapper start-->

        <!-- shop start -->
          <?php include './pages/search-result/blocks/result.php' ?>
        <!-- shop end -->

      </div>
    </div>
  </div>
</div>
<!--shop  area end-->
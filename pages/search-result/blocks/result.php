<?php
$query = $_GET["q"];
$brand_name = $_GET["brand"] ?? '';
?>

<div class="shop_toolbar_wrapper">
  <div class="shop_toolbar_btn">
    <button data-role="grid_3" type="button" class=" btn-grid-3" data-toggle="tooltip" title="3"></button>
    <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip" title="4"></button>
    <button data-role="grid_list" type="button" class="active btn-list" data-toggle="tooltip" title="List"></button>
  </div>
  <!--  <div class=" niceselect_option">-->
  <!--    <select class="address-dropdown" id="product_short">-->
  <!--      <option value="new" selected>Sort by: Newness</option>-->
  <!--      <option value="low">Sort by price: Low to high</option>-->
  <!--      <option value="high">Sort by price: High to low</option>-->
  <!--      <option value="name_desc">Product Name: A-Z</option>-->
  <!--    </select>-->
  <!--  </div>-->
  <div class="page_amount" style="text-align: center">
    <p id="total-record"></p>
  </div>
</div>

<div id="search-result">
  <p id="search-success" style="font-size: 16px">Result for: '<strong style="color: #40A944"><?php echo $query;
          ?></strong>'</p>
  <p id="search-fail" style="font-size: 16px">Sorry, we couldn't find any for '<strong style="color: #40A944"><?php echo
          $query;
          ?></strong>'</p>
</div>

<div id="product-shop-container">
  <div id="product-toolbar-container" class="row shop_wrapper grid_list"></div>
  <div id="product-toolbar-pagination" class="shop_toolbar t_bottom">
    <div class="pagination">
      <ul id="pagination"></ul>
    </div>
  </div>
</div>

<script>
  const query = '<?php echo $query; ?>'
  const brandSearchId = '<?php echo $brand_name; ?>'

  const ajax = new XMLHttpRequest()
  const async = true
  const brandBaseUrl = 'model/product'
  const allSearchProductUrl = `${brandBaseUrl}/search-product-with-pagination.php?q=${query}&brand=${brandSearchId}`

  const rangeFrom = document.getElementById('range-from')
  const rangeTo = document.getElementById('range-to')

  const parentElement = document.getElementById('product-shop-container')
  const toolbarElement = document.getElementById('product-toolbar-container')
  const paginationElement = document.getElementById('product-toolbar-pagination')
  const searchSuccessElement = document.getElementById('search-success')
  const searchFailElement = document.getElementById('search-fail')
  const totalRecord = document.getElementById('total-record')
  const paginationNumber = document.getElementById('pagination')

  const addZeroes = (num) => {
    let value = Number(num)
    const res = num.split('.')
    if (res.length === 1 || (res[1].length < 3)) {
      value = value.toFixed(2)
    }
    return value
  }

  const mapDataToDisplay = (data) => {
    toolbarElement.innerHTML = data.map((val) => {
      return `
        <div class="col-12">
          <div class="single_product">
            <div class="product_thumb">
              <a class="primary_img" href="index.php?p=product-detail&id=${val.id}"><img
                    style="height: 100%;" src="${val.image}" alt="${val.id}-${val.name}"></a>
              <div class="label_product">
                <span class="label_sale">Sale</span>
                <span class="label_new">New</span>
              </div>
              <div class="action_links">
                <ul>
                  <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id=${val.id}" data-tippy="Add to cart" data-tippy-placement="top"
                                             data-tippy-arrow="true" data-tippy-inertia="true"> <span
                          class="lnr lnr-cart"></span></a></li>
                  <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-tippy-placement="top"
                                          data-tippy-arrow="true" data-tippy-inertia="true"><span
                          class="lnr lnr-heart"></span></a></li>
                </ul>
              </div>
            </div>
            <div class="product_content grid_content">
              <h4 class="product_name"><a
                    href="index.php?p=product-detail&id=${val.id}">${val.name}</a></h4>
              <p>
                <a href="index.php?p=brand&slug=${val.cslug ? val.cslug : val.bslug}&page=">${val.cname
        ? val.cname
        : val.bname}</a>
              </p>
              <div class="price_box">
                <span class="current_price">USD ${addZeroes(`${val.sale_price}`)}</span>
                <span class="old_price">USD ${addZeroes(`${val.price}`)}</span>
              </div>
            </div>
            <div class="product_content list_content">
              <h4 class="product_name"><a
                    href="index.php?p=product-detail&id=${val.id}">${val.name}</a></h4>
              <p>
                <a href="index.php?p=brand&slug=${val.cslug ? val.cslug : val.bslug}&page=">${val.cname
        ? val.cname
        : val.bname}</a>
              </p>
              <div class="price_box">
                <span class="current_price">USD ${addZeroes(`${val.sale_price}`)}</span>
                <span class="old_price">USD ${addZeroes(`${val.price}`)}</span>
              </div>
              <div class="product_desc">${val.intro}</div>
              <div class="action_links list_action_right">
                
              </div>
            </div>
          </div>
        </div>
      `
    }).join('')
  }

  const fetchBrandFilterData = (url, page = null) => {
    ajax.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        const response = JSON.parse(this.responseText)
        paginationNumber.innerHTML = ''
        if (response.length <= 0) {
          paginationElement.style.display = 'none'
          searchFailElement.style.display = 'block'
          searchSuccessElement.style.display = 'none'
        } else {
          totalRecord.innerHTML = `We have found ${response.totalRecord} ${response.totalRecord > 1 ? 'results' :
            'result'}`
          paginationElement.style.display = 'flex'
          searchFailElement.style.display = 'none'
          searchSuccessElement.style.display = 'block'
          for (let i = 1; i <= (Math.ceil(response.totalRecord / 5)); i++) {
            paginationNumber.innerHTML += `<li>
          <a onclick="onMoveToPageHandler(${i})"
             class="links ${(i === page) ? 'pagination-is-clicked' : ''}">${i}</a>
        </li>`
          }
          mapDataToDisplay(response.data)
        }
      }
    }
    ajax.open('GET', url, async)
    ajax.send()
  }

  fetchBrandFilterData(allSearchProductUrl, 1)

  const onMoveToPageHandler = (page) => {
    let paginationItem = document.getElementsByClassName('a')
    for (let i = 0; i < paginationItem.length; i++) {
      paginationItem[i].classList.remove('pagination-is-clicked')
    }

    const moveToPage = `${brandBaseUrl}/search-product-with-pagination.php?q=${query}&brand=${brandSearchId}&page=${page}`
    fetchBrandFilterData(moveToPage, page)
  }

  // const onFilterBtnClick = () => {
  //   const rangeFromValue = rangeFrom.value.replace('$', '')
  //   const rangeToValue = rangeTo.value.replace('$', '')
  //   const filterByPriceUrl = `${brandBaseUrl}/get-product-category-by-price.php?slug=${slug}&start=${start}&end=${end
  //   }&from=${rangeFromValue}&to=${rangeToValue}`
  //   fetchBrandFilterData(filterByPriceUrl)
  // }
</script>
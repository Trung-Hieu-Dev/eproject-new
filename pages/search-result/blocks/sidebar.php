<?php
$one_product = all_product($conn);

function get_all_category($conn)
{
    $stmt = $conn->prepare("SELECT * FROM category WHERE parent = 3");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

$category = get_all_category($conn);
?>

<aside class="sidebar_widget">
  <div class="widget_inner">
    <div class="widget_list widget_color">
      <h3>Select By Brand</h3>
      <ul>
          <?php
          $brands = list_brand($conn);
          foreach ($brands as $item) {
              ?>
            <li>
              <a href="index.php?p=brand&slug=<?php echo $item['slug'] ?>"><?php echo $item['name'] ?>
              </a>
            </li>
          <?php } ?>
      </ul>
    </div>

    <!--    <div class="widget_list widget_filter">-->
    <!--      <h3>Filter by price</h3>-->
    <!--      <div class="form">-->
    <!--        <div id="slider-range"></div>-->
    <!--        <div class="range-item">-->
    <!--          <button onclick="onFilterBtnClick()" name="filter_price" type="submit">Filter</button>-->
    <!--          <div class="range-price-item">-->
    <!--            <input id="range-from" name="range_from" />-->
    <!--            <p style="margin: 0 5px">-</p>-->
    <!--            <input id="range-to" name="range_to" />-->
    <!--          </div>-->
    <!--        </div>-->
    <!--      </div>-->
    <!--    </div>-->

    <div class="widget_list widget_color">
      <h3>Select By Category</h3>
      <ul>
          <?php
          $category = get_all_category($conn);
          foreach ($category as $item) {
              ?>
            <li>
              <a href="index.php?p=shop-list&slug=<?php echo $item['slug'] ?>"><?php echo $item["name"] ?></a>
            </li>
          <?php } ?>
      </ul>
    </div>

    <div class="widget_list banner_widget">
      <div class="banner_thumb">
          <?php
          foreach ($one_product as $item) {
              if ($item['id'] == 8) {
                  echo '<a href="#"><img src="./public/assets/image-product/' . $item["image"] . '" alt=""></a>';
              }
          }
          ?>
      </div>
    </div>
  </div>
</aside>
<?php



include './core/function.php';
include './model/auth.php';





if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
  header('location:index.php');
  exit;
}

if (isset($_SESSION["customer_profile"])) {
  $old_data = $_SESSION["customer_profile"];
  $user_id = $old_data["id"];
  $province = $old_data["province_city"];
  $district = $old_data["district"];
  $ward = $old_data["ward"];
  $address_detail = $old_data["address_detail"];
}

$errors_login = array();
$errors = array();
$success = false;
$create_account = null;

function display_cart_product($conn, $id)
{
  $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
  $stmt->bindParam(':id', $id, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
}

//    login
if (isset($_POST["login"])) {
  if (empty($_POST["email"])) {
    $errors_login[] = "Please enter a email address.";
  } else {
    $email = $_POST["email"];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors_login[] = "Invalid email address.";
    }
  }

  if (empty($_POST["password"])) {
    $errors_login[] = "Please enter your password";
  }

  if (empty($errors_login)) {
    $data = array(
      "email" => $_POST["email"],
      "password" => $_POST["password"]
    );
    $response = customer_login($conn, $data);
    if ($response) {
      $_SESSION["customer_profile"] = $response[0];
      header("location:index.php?p=checkout");
      exit();
    } else {
      $errors_login[] = 'Email or password incorrect';
    }
  }
}

//Oder Start
if (isset($_POST["order"])) {

  //      full name
  if (empty($_POST["full_name"])) {
    $errors[] = "Please enter your full name.";
  } else {
    $name = $_POST["full_name"];
    if (!preg_match("/^([a-zA-Z' ]+)$/", $name)) {
      $errors[] = "Full name does not contain number or special characters.";
    }
  }

  //        phone
  if (empty($_POST["phone"])) {
    $errors[] = "Please enter phone number.";
  }

  if (!is_numeric($_POST["phone"])) {
    $errors[] = "Phone number must be a number.";
  }

  if (strlen($_POST["phone"]) > 11 || strlen($_POST["phone"]) < 10) {
    $errors[] = "Phone number must be between 10 and 11 characters.";
  }

  //    email
  if (empty($_POST["email"])) {
    $errors[] = "Please enter a email address.";
  } else {
    $email = $_POST["email"];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors[] = "Invalid email address.";
    }
  }

  //        address
  if (!empty($_POST["province_city"])) {
    if (empty($_POST["district"])) {
      $errors[] = "Please select a district.";
    }
    if (empty($_POST["ward"])) {
      $errors[] = "Please select a ward.";
    }
    if (empty($_POST["address_detail"])) {
      $errors[] = "Please address details (Apartment No., Street).";
    }
  }

  if (empty($_POST["province_city"])) {
    $errors[] = "Please select a province or a city.";
  }
  if (empty($_POST["district"])) {
    $errors[] = "Please select a district.";
  }
  if (empty($_POST["ward"])) {
    $errors[] = "Please select a ward.";
  }
  if (empty($_POST["address_detail"])) {
    $errors[] = "Please address details (Apartment No., Street).";
  }

  //        password
  if (isset($_POST["create-account-check"]) && $_POST["create-account-check"] == 'on') {
    $password = $_POST["password"];
    if (strlen($password) < 8) {
      $errors[] = "Password must be at least 8 characters.";
    }
    if (!preg_match("/\d/", $password)) {
      $errors[] = "Password must be contain at least one number.";
    }
    if (!preg_match("/[A-Z]/", $password)) {
      $errors[] = "Password muse be contain at least one uppercase letter.";
    }
    if (!preg_match("/[a-z]/", $password)) {
      $errors[] = "Password must be contain at least one lowercase letter.";
    }
    if (!preg_match("/\W/", $password)) {
      $errors[] = "Password must be contain at least one special character.";
    }
    if (preg_match("/\s/", $password)) {
      $errors[] = "Password should not contain any white space.";
    }
    if (empty($_POST["confirm_password"])) {
      $errors[] = "Please enter a confirm password.";
    } else {
      if ($_POST['password'] !== $_POST['confirm_password']) {
        $errors[] = "Password does not match.";
      }
    }
  }

  if (empty($errors)) {
    include './sendmail.php';
    
    $data = array(
      "email" => $_POST["email"],
      "phone" => $_POST["phone"],
      "full_name" => $_POST["full_name"],
      "ward" => $_POST["ward"],
      "district" => $_POST["district"],
      "province_city" => $_POST["province_city"],
      "address_detail" => $_POST["address_detail"]
    );

    foreach ($_SESSION['cart'] as $id => $quantity) {
      $product_in_cart = display_cart_product($conn, $id);
      $data["cart"][$id] = array(
        "product_price" => $product_in_cart["sale_price"] ?? $product_in_cart["price"],
        "quantity" => $quantity,
        "total_price" => ($product_in_cart["sale_price"] ?? $product_in_cart["price"]) * $quantity,
        "product_id" => $id
      );
    }


    if (isset($_POST["notes"])) {
      $data["notes"] = $_POST["notes"];
    }

    if (isset($user_id)) {
      $data["id"] = $user_id;
    }

    if (isset($_POST["create-account-check"])) {
      $data["password"] = $_POST["password"];
      $create_account = $_POST["create-account-check"];
    }

    checkout($conn, $data, $create_account, $errors[], $success);

    foreach ($errors as $key => $error) {
      if ($error == '') {
        unset($errors[$key]);
      }
    }

        
    
  }
  
}
?>

<!--breadcrumbs area start-->
<?php include './pages/checkout/blocks/breadcrumb.php' ?>
<!--breadcrumbs area end-->

<!--Checkout page section-->
<div class="Checkout_section mt-70">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php if (!isset($_SESSION["customer_profile"])) { ?>
          <!-- Alert start -->
          <?php if (!empty($errors_login)) { ?>
            <div class="alert alert-danger alert-dismissible">
              <h5><span class="lnr lnr-cross-circle"></span> Alert!</h5>
              <?php
              foreach ($errors_login as $error) {
                echo '<li>' . $error . '</li>';
              }
              ?>
            </div>
          <?php } ?>
          <!-- /.alert -->

          <!--              login-->
          <!-- <div class="user-actions">
            <h3>
              <i class="fa fa-file-o" aria-hidden="true"></i>
              Returning customer?
              <a class="Returning" href="#checkout_login" data-bs-toggle="collapse" aria-expanded="true">Click here
                to login</a>
            </h3>
            <div id="checkout_login" class="collapse" data-parent="#accordion">
              <div class="checkout_info">
                <p>If you have shopped with us before, please enter your details in the boxes below. If you are a
                  new customer please proceed to the Billing & Shipping section.</p>
                <form action="" method="POST">
                  <div class="form_group">
                    <label>Email Address <span>*</span></label>
                    <input type="text" name="email">
                  </div>
                  <div class="form_group">
                    <label>Password <span>*</span></label>
                    <input type="password" name="password">
                  </div>
                  <div class="form_group group_3 ">
                    <button type="submit" name="login">Login</button>
                  </div>
                  <a href="forgot-password.php">Lost your password?</a>
                </form>
              </div>
            </div>
          </div> -->
        <?php } ?>
      </div>
    </div>
    <form action="" method="POST">
      <div class="checkout_form">
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <h3>Billing Details</h3>
            <div class="row">
              <div class="col-12">
                <!-- Alert start -->
                <?php if (!empty($errors)) { ?>
                  <div class="alert alert-danger alert-dismissible">
                    <h5><span class="lnr lnr-cross-circle"></span> Alert!</h5>
                    <?php
                    foreach ($errors as $error) {
                      echo '<li>' . $error . '</li>';
                    }
                    ?>
                  </div>
                <?php } ?>
                <!-- /.alert -->
              </div>
            </div>
            <div class="row">
              <!--              full name-->
              <div class="col-12">
                <label>Full Name <span>*</span></label>
                <input type="text" name="full_name" <?php isset($old_data) ? keep_value_input('full_name', $old_data["full_name"]) : keep_value_input('full_name');
                                                    ?>>
              </div>
              <!--              add: detail-->
              <div class="col-12 mb-20 address-title flex-column">
                <?php if (isset($old_data)) {
                  if ($address_detail && $ward && $district && $province) { ?>
                    <div class="label">
                      <label>Address <span>*</span></label>
                      <label class="edit-address-btn" onclick="onBtnExpandEditAddress()">Edit Address Here</label>
                    </div>
                    <input type="text" disabled value="<?php display_address($conn, $address_detail, $ward, $district, $province);
                                                        ?>" />
                <?php }
                } ?>
              </div>
              <div id="edit-address-detail" class="edit-address-detail <?php if (
                                                                          !$address_detail && !$ward && !$district &&
                                                                          !$province
                                                                        ) {
                                                                          echo 'edit-address-active';
                                                                        } ?>">
                <div class="row">
                  <!--add: province/city-->
                  <div class="col-6 mb-20">
                    <label>Province/city <span>*</span></label>
                    <select class="address-dropdown" name="province_city" id="province" onchange="addressCodeSelect('province')">
                      <option value="" selected disabled>Select a province/city</option>
                      <?php
                      if (isset($_POST["province_city"])) {
                        keep_province_address_option($conn, $_POST["province_city"]);
                      } else {
                        keep_province_address_option($conn, $province);
                      }
                      ?>
                    </select>
                  </div>
                  <!--add: district-->
                  <div class="col-6 mb-20">
                    <label>District <span>*</span></label>
                    <select class="address-dropdown" name="district" id="district" onchange="addressCodeSelect
                                                ('district')">
                      <option value="" selected disabled>Select a district</option>
                      <?php
                      if (isset($_POST["district"]) && isset($_POST["province_city"])) {
                        keep_district_address_option($conn, $_POST["province_city"], $_POST["district"]);
                      } else {
                        keep_district_address_option($conn, $province, $district);
                      }
                      ?>
                    </select>
                  </div>
                  <!--add: ward-->
                  <div class="col-6 mb-20">
                    <label>Ward <span>*</span></label>
                    <select class="address-dropdown" id="ward" name="ward">
                      <option value="" selected disabled>Select a ward</option>
                      <?php
                      if (isset($_POST["district"]) && isset($_POST["province_city"]) && isset($_POST["ward"])) {
                        keep_ward_address_option($conn, $_POST["district"], $_POST["ward"]);
                      } else {
                        keep_ward_address_option($conn, $district, $ward);
                      }
                      ?>
                    </select>
                  </div>
                  <!--add: house number-->
                  <div class="col-6 mb-20">
                    <label>House No., street <span>*</span></label>
                    <input type="text" name="address_detail" <?php isset($old_data) ? keep_value_input(
                                                                'address_detail',
                                                                $address_detail
                                                              ) : keep_value_input('address_detail');
                                                              ?>>
                  </div>
                </div>
              </div>
              <!--              phone-->
              <div class="col-lg-6 mb-20">
                <label>Phone<span>*</span></label>
                <input type="text" name="phone" <?php isset($old_data) ? keep_value_input('phone', $old_data["phone"]) : keep_value_input('phone'); ?>>
              </div>
              <!--              email-->
              <div class="col-lg-6 mb-20">
                <label> Email Address <span>*</span></label>
                <input type="text" name="email" <?php if (isset($old_data)) {
                                                  echo 'value="' . $old_data["email"] . '"';
                                                } else {
                                                  keep_value_input('email');
                                                } ?>>
              </div>
              
              <div class="col-12">
                
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6">
            <h3>Your order</h3>
            <div class="order_table table-responsive">
              <table>
                <thead>
                  <tr>
                    <th>Product</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $total = 0;
                  foreach ($_SESSION['cart'] as $id => $quantity) {
                    $product = display_cart_product($conn, $id)
                  ?>
                    <tr>
                      <td> <?php echo $product['name'] ?> <strong> × <?php echo $quantity ?></strong></td>
                      <td>USD <?php
                              $total += $product['sale_price'] * $quantity;
                              echo number_format($product['sale_price'] * $quantity, "0", "", ".")
                              ?></td>
                    </tr>
                  <?php } ?>
                  <tr class="order_total">
                    <th style="border-bottom: 1px solid gainsboro;">Order Total</th>
                    <td><strong>USD <?php echo number_format($total, "0", "", ".") ?></strong></td>
                  </tr>
                  </tfoot>
              </table>
            </div>
            <div class="payment_method">
              <div class="order_button">
                <button type="submit" name="order">Proceed to PayPal</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<!--Checkout page section end-->

<script>
  const createPassword = document.getElementById('create-password')
  const onCreateAccount = (cb) => {
    if (cb.checked) {
      createPassword.style.display = 'block'
    } else {
      createPassword.style.display = 'none'
    }
  }
</script>
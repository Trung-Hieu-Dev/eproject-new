<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <h3>Cart</h3>
                    <ul>
                        <li><a href="index.html">home</a></li>
                        <li>Shopping Cart</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--breadcrumbs area end-->

<!--shopping cart area start -->
<div class="shopping_cart_area mt-70">
    <div class="container">
        <form action="" method="POST">
            <div class="row">
                <div class="col-12">
                    <div class="table_desc">
                        <div class="cart_page">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product_remove">Delete</th>
                                        <th class="product_thumb">Image</th>
                                        <th class="product_name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product_quantity">Quantity</th>
                                        <th class="product_total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total = 0;
                                    if (isset($_SESSION['cart'])) {
                                        foreach ($_SESSION['cart'] as $id => $quantity) {


                                            $stmt = $conn->prepare("SELECT * FROM product WHERE id = :id");
                                            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                                            $stmt->execute();
                                            $product = $stmt->fetch(PDO::FETCH_ASSOC);

                                    ?>
                                            <tr>
                                                <td class="product_remove"><a href="" class="removeItemCart" data-id="<?php echo $id ?>"><i class="fa fa-trash-o"></i></a></td>
                                                <td class="product_thumb"><a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><img src="<?php echo $product['image'] ?>" alt=""></a></td>
                                                <td class="product_name"><a href="index.php?p=product-detail&id=<?php echo $product['id'] ?>"><?php echo $product['name'] ?></a></td>
                                                <td class="product-price">USD <?php echo number_format($product['sale_price'], "0", "", ".") ?></td>
                                                <td class="product_quantity"><label>Quantity</label> <input min="1" max="100" value="<?php echo $quantity ?>" type="number" name="upDateCart" data-id="<?php echo $id ?>" class="upDateCart"></td>
                                                <td class="product_total">USD <?php
                                                                                $total += $product['sale_price'] * $quantity;
                                                                                echo number_format($product['sale_price'] * $quantity, "0", "", ".")
                                                                                ?></td>


                                            </tr>
                                    <?php }
                                    } ?>


                                </tbody>
                            </table>
                        </div>
                        <div class="checkout_btn p-0">
                            <a href="index.php" style="margin: 5px; border-radius:3px">Shop more</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--coupon code area start-->
            <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">USD <?php echo number_format($total, "0", "", ".") ?></p>
                                </div>
                                <div class="checkout_btn">
                                    <a href="index.php?p=checkout">Proceed to Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--coupon code area end-->
        </form>
    </div>
</div>
<!--shopping cart area end -->
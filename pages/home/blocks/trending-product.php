<?php

$child_category = category($conn);

?>



<!--product area start-->
<div class="product_area  mb-64">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="product_header">
                    <div class="section_title">
                        <p>Recently added our store</p>
                        <h2>Trending Products</h2>
                    </div>
                    <div class="product_tab_btn">
                        <ul class="nav" role="tablist" id="nav-tab">
                            <!-- Choose child category start -->
                            <?php foreach ($child_category as $category) { ?>
                                <li>
                                    <!-- Choose attribute start -->
                                    <a data-toggle="tab" href="#<?php
                                                                // Choose #plant for href start 
                                                                if ($category['slug'] == 'dresses') {
                                                                    $href_attribute = 'plant1';
                                                                    echo $href_attribute;
                                                                } elseif ($category['slug'] == 'bags') {
                                                                    $href_attribute = 'plant2';
                                                                    echo $href_attribute;
                                                                } else {
                                                                    $href_attribute = 'plant3';
                                                                    echo $href_attribute;
                                                                } // Choose plant #plant for href end
                                                                ?>" role="tab" aria-controls="<?php
                                                                                                // Choose #plant for aria-controls start 
                                                                                                if ($category['slug'] == 'dresses') {
                                                                                                    $controls = 'plant1';
                                                                                                    echo $controls;
                                                                                                } elseif ($category['slug'] == 'bags') {
                                                                                                    $controls = 'plant2';
                                                                                                    echo $controls;
                                                                                                } else {
                                                                                                    $controls = 'plant3';
                                                                                                    echo $controls;
                                                                                                } // Choose plant #plant for aria-controls end
                                                                                                ?>">



                                        <?php echo $category['name'] ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <!-- Choose category child end -->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="product_container">
            <div class="row">
                <div class="col-12">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="plant1" role="tabpanel">
                            <?php
                            $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 7 ORDER BY RAND() LIMIT 5");
                            $stmt->execute();
                            $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                            ?>
                            <div class="product_carousel product_column5 owl-carousel">
                                <?php foreach ($products as $item) { ?>
                                    <div class="product_items">
                                        <article class="single_product">
                                            <figure>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                                    <div class="label_product">
                                                        <span class="label_sale">Sale</span>
                                                        <span class="label_new">New</span>
                                                    </div>
                                                    <div class="action_links">
                                                        <ul>
                                                            <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                            <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <figcaption class="product_content">
                                                    <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                                    <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                                    <div class="price_box">
                                                        <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                                        <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>
                                <?php } ?>
                            </div>

                        </div>

                        <div class="tab-pane fade" id="plant2" role="tabpanel">
                            <?php
                            $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 6 ORDER BY RAND() LIMIT 5");
                            $stmt->execute();
                            $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                            ?>
                            <div class="product_carousel product_column5 owl-carousel">
                                <?php foreach ($products as $item) { ?>
                                    <div class="product_items">
                                        <article class="single_product">
                                            <figure>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                                    <div class="label_product">
                                                        <span class="label_sale">Sale</span>
                                                        <span class="label_new">New</span>
                                                    </div>
                                                    <div class="action_links">
                                                        <ul>
                                                            <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                            <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <figcaption class="product_content">
                                                    <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                                    <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                                    <div class="price_box">
                                                        <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                                        <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="plant3" role="tabpanel">
                            <?php
                            $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 5 ORDER BY RAND() LIMIT 5");
                            $stmt->execute();
                            $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                            ?>
                            <div class="product_carousel product_column5 owl-carousel">
                                <?php foreach ($products as $item) { ?>
                                    <div class="product_items">
                                        <article class="single_product">
                                            <figure>
                                                <div class="product_thumb">
                                                    <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                                    <div class="label_product">
                                                        <span class="label_sale">Sale</span>
                                                        <span class="label_new">New</span>
                                                    </div>
                                                    <div class="action_links">
                                                        <ul>
                                                            <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                            <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <figcaption class="product_content">
                                                    <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                                    <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                                    <div class="price_box">
                                                        <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                                        <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </article>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--product area end-->
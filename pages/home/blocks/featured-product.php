<?php

$products = get_all_product($conn);


?>

<!--custom product area start-->
<div class="custom_product_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <p>Recently added our store </p>
                    <h2>Featured Products</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="small_product_area product_carousel product_column3 owl-carousel">
                    <div class="product_items">
                        <?php
                        $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 7 ORDER BY RAND() LIMIT 3");
                        $stmt->execute();
                        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        ?>
                        <?php foreach ($products as $item) { ?>
                            <article class="single_product">
                                <figure>
                                    <div class="product_thumb">
                                        <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                    </div>
                                    <figcaption class="product_content">
                                        <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                        <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                        <div class="action_links">
                                            <ul>
                                                <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                            <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </article>
                        <?php } ?>

                    </div>
                    <div class="product_items">
                        <?php
                        $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 6 ORDER BY RAND() LIMIT 3");
                        $stmt->execute();
                        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        ?>
                        <?php foreach ($products as $item) { ?>
                            <article class="single_product">
                                <figure>
                                    <div class="product_thumb">
                                        <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                    </div>
                                    <figcaption class="product_content">
                                        <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                        <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                        <div class="action_links">
                                            <ul>
                                                <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                            <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </article>
                        <?php } ?>

                    </div>
                    <div class="product_items">
                        <?php
                        $stmt = $conn->prepare("SELECT p.*,c.name as cname, c.slug as cslug  FROM product as p, category as c WHERE p.category_id = c.id AND c.id = 5 ORDER BY RAND() LIMIT 3");
                        $stmt->execute();
                        $products = $stmt->fetchAll(PDO::FETCH_ASSOC);

                        ?>
                        <?php foreach ($products as $item) { ?>
                            <article class="single_product">
                                <figure>
                                    <div class="product_thumb">
                                        <a class="primary_img" href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><img src="<?php echo $item['image'] ?>" alt=""></a>
                                    </div>
                                    <figcaption class="product_content">
                                        <h4 class="product_name"><a href="index.php?p=product-detail&id=<?php echo $item['id'] ?>"><?php echo $item['name'] ?></a></h4>
                                        <p><a href="index.php?p=shop-list&slug=<?php echo $item['cslug'] ?>"><?php echo $item['cname'] ?></a></p>
                                        <div class="action_links">
                                            <ul>
                                                <li class="add_to_cart"><a href="" class="adToCart" name="quantity" value=1 data-id="<?php echo $item['id'] ?>" data-tippy="Add to cart" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"> <span class="lnr lnr-cart"></span></a></li>
                                                <li class="wishlist"><a href="#" data-tippy="Add to Wishlist" data-id="<?php echo $item['id'] ?>" data-tippy-placement="top" data-tippy-arrow="true" data-tippy-inertia="true"><span class="lnr lnr-heart"></span></a></li>
                                            </ul>
                                        </div>
                                        <div class="price_box">
                                            <span class="current_price">USD <?php echo number_format($item['sale_price']) ?></span>
                                            <span class="old_price">USD <?php echo number_format($item['price']) ?></span>
                                        </div>
                                    </figcaption>
                                </figure>
                            </article>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--custom product area end-->
<?php

$brands = list_brand($conn);


?>

<div class="brand_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="brand_container owl-carousel ">
                    <?php
                        foreach ($brands as $item) {
                            echo '<div class="single_brand">
                                        <a href="index.php?p=brand&slug='.$item['slug'].'"><img src="./ap-admin/public/assets/brand_image/'.$item["image"].'" alt=""></a>
                                    </div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

$sliders = list_slider($conn);


?>

<section class="slider_section">
    <div class="slider_area owl-carousel">
        <?php foreach ($sliders as $item) { 
            if($item['status'] == 1) {
        ?>
            <div class="single_slider d-flex align-items-center" data-bgimg="./ap-admin/public/assets/brand_image/<?php echo $item["image"] ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="slider_content">
                                <h1>Black Friday</h1>
                                <h2>Sale Off 30%</h2>
                                <p>
                                    Only for 24 hours
                                </p>
                                <a href="index.php?p=shop-list">Read more </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php } ?>
    </div>
</section>
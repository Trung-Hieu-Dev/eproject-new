<?php

$banners = list_banner($conn);

?>


<div class="banner_area">
    <div class="container">
        <div class="row">
            <?php foreach ($banners as $item) { ?>
                <div class="col-lg-6 col-md-6">
                    <div class="single_banner">
                        <div class="banner_thumb">
                            <a href="index.php?p=shop-list.php"><img src="./ap-admin/public/assets/brand_image/<?php echo $item["image"] ?>" alt=""></a>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
<?php
session_start();
ob_start();

include './core/config.php';
include './core/connect.php';
include './model/user.php';
include './model/product/product.php';
include './model/slider.php';
include './model/banner.php';
include './model/brand.php';
include './model/category.php';
include './model/cart.php';
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php include './blocks/head.php' ?>
</head>

<body>

    <!--header area start-->

    <!--offcanvas menu area start-->
    <?php include './blocks/canvas-menu.php' ?>
    <!--offcanvas menu area end-->

    <?php include './blocks/header.php' ?>

    <!--header area end-->

    <!-- <?php include './blocks/slider.php'?> -->

    <!-- main content start -->
    <?php
    if (isset($_GET['p'])) {
        $p = $_GET["p"];

        switch ($p) {
            case 'home-page':
                include './pages/home/home.php';
                break;

            case 'shop-list':
                include './pages/product/product.php';
                break;

            case 'brand':
                include './pages/brand/brand.php';
                break;

            case 'product-detail':
                include './pages/product/detail.php';
                break;

            case 'cart':
                include './pages/cart/cart.php';
                break;

            case 'checkout':
                include './pages/checkout/checkout.php';
                break;

            case 'wishlist':
                include './pages/wishlist/wishlist.php';
                break;

            case 'contact':
                include './pages/contact/contact.php';
                break;

            case 'about':
                include './pages/about/about.php';
                break;
            case 'bill':
                include './pages/bill/bill.php';
                break;

        case 'search':
            include './pages/search-result/search-result.php';
            break;

        default:
            header("location:error.php");
            exit();
        }
    } else {
        include './pages/home/home.php';
    }


    ?>
    <!-- main content end -->

    <!--footer area start-->
    <?php include './blocks/footer.php' ?>
    <!--footer area end-->

    <!-- modal area start-->
    <?php include './blocks/modal.php' ?>
    <!-- modal area end-->

    <!--news letter popup start-->
    <!-- <?php include './blocks/popup.php' ?> -->
    <!--news letter popup start-->


    <?php include './blocks/foot.php' ?>


</body>



</html>
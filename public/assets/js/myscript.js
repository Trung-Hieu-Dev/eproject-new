$(document).ready(function () {
  // btn confirm logout
  $('.confirmLogout').click(function () {
    if (confirm('Are you sure you want to log out?')) {
      return true
    }
    return false
  })

  //Cart start
  $('#adToCart').click(function (e) {
    e.preventDefault()
    var quantityJS = $('input[name="quantity"]').val()
    var product_idJS = $('input[name="quantity"]').data('id')
    $.ajax({
      type: 'POST',
      url: './ajax/cart.php',
      data: { product_id: product_idJS, quantity: quantityJS },
      dataType: 'html',
    })
  })

  $('.removeItemCart').click(function (e) {
    e.preventDefault()
    var idJS = $(this).data('id')
    $.ajax({
      type: 'GET',
      url: './ajax/cart.php',
      data: { delete: idJS },
      dataType: 'html',
      success: function (response) {
        window.location.reload()

      },
    })
  })

  $('.upDateCart').click(function (e) {
    e.preventDefault()
    var quantityJS = $(this).val()
    var product_idJS = $(this).data('id')
    $.ajax({
      type: 'POST',
      url: './ajax/cart.php',
      data: { product_id: product_idJS, quantity: quantityJS },
      dataType: 'html',
      success: function (response) {
        window.location.reload()

      },

    })
  })

  // $('#upDateCart').click(function (e) {
  //   e.preventDefault();
  //   var quantityJS = $('input[name="upDateCart"]').val();
  //   var product_idJS = $('input[name="upDateCart"]').data('id');
  //   $.ajax({
  //     type: "POST",
  //     url: "./ajax/cart.php",
  //     data: { product_id: product_idJS, quantity: quantityJS },
  //     dataType: "html",
  //     success: function (response) {
  //       // window.location.reload();

  //     }

  //   });
  // });

  $('.adToCart').click(function () {
    var id = $(this).data('id')
    $.ajax({
      type: 'GET',
      url: './ajax/cart.php',
      data: { plus_cart: id },
      dataType: 'html',
      success: function (response) {
        window.location.reload()
        

      },

    })

  })
//Wishlist
$('.add-to-wishlist,[data-tippy="Add to Wishlist"]').click(function (e) {
  e.preventDefault();
  var product_idJS = $(this).data("id");
  $.ajax({
    type: "POST",
    url: "./ajax/wishlist.php",
    data: { product_id: product_idJS, quantity: 1 },
    dataType: "html",
  });
});

$('.removeItemWishlist').click(function (e) {
  e.preventDefault();
  var idJS = $(this).data('id');
  $.ajax({
    type: "GET",
    url: "./ajax/wishlist.php",
    data: { delete: idJS },
    dataType: "html",
    success: function (response) {
      window.location.reload();

    }
  });
});
// $('.upDateWishlist').click(function (e) {
//   e.preventDefault();
//   var quantityJS = $(this).val();
//   var product_idJS = $(this).data('id');
//   $.ajax({
//     type: "POST",
//     url: "./ajax/wishlist.php",
//     data: { product_id: product_idJS, quantity: quantityJS },
//     dataType: "html",
//     success: function (response) {
//       window.location.reload();

//     }
//   });
// });
$('.wishlist').click(function () {
  var id = $(this).data('id');
  $.ajax({
    type: "GET",
    url: "./ajax/wishlist.php",
    data: { plus_wishlist: id },
    dataType: "html",
    success: function (response) {
      window.location.reload();

    }

  });
  
});
})

// CREATE USER ADDRESS
// initial ajax
const ajax = new XMLHttpRequest()
const baseUrl = 'model/address/'
let urlToQuery = `${baseUrl}/getProvince.php`
const async = true
const optionProvince = document.getElementById('province')
const optionDistrict = document.getElementById('district')
const optionWard = document.getElementById('ward')

// receive response
const appendAddressOption = (type, code, alreadyCode) => {
  ajax.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      const response = JSON.parse(this.responseText)
      type.innerHTML += response.map((val) => {
        switch (code) {
          case 'maqh':
            return `<option id="${code}" value="${val.maqh}">${val.name}</option>`
          case 'xaid':
            return `<option id="${code}" value="${val.xaid}">${val.name}</option>`
          default:
            return `<option value="${val.matp}">${val.name}</option>`
        }
      }).join('')
    }
  }
  ajax.open('GET', urlToQuery, async)
  ajax.send()
}

// remove option element
const removeOptionElement = (parent) => {
  const len = parent.options.length - 1
  for (let i = len; i >= 0; i--) {
    if (parent.options[i].id !== '') {
      parent.remove(i)
    }
  }
  parent.selectedIndex = 0
}

// initial province options
if (optionProvince.childElementCount <= 1) {
  appendAddressOption(optionProvince, 'matp')
}

// onchange to show relate district and ward
const addressCodeSelect = (type) => {
  if (type === 'province') {
    removeOptionElement(optionDistrict)
    if (optionWard.childElementCount > 1) {
      removeOptionElement(optionWard)
    }
    const matp = optionProvince.value
    urlToQuery = `${baseUrl}/getDistrictBaseOnProvince.php?q=${matp}`
    appendAddressOption(optionDistrict, 'maqh')
  } else if (type === 'district') {
    removeOptionElement(optionWard)
    const maqh = optionDistrict.value
    urlToQuery = `${baseUrl}/getWardBaseOnDistrict.php?q=${maqh}`
    appendAddressOption(optionWard, 'xaid')
  }
}

let isActive = false
const onBtnExpandEditAddress = () => {
  const addClass = document.getElementById('edit-address-detail')
  isActive = !isActive
  if (isActive) {
    addClass.classList.add('edit-address-active')
  } else {
    addClass.classList.remove('edit-address-active')
  }
}

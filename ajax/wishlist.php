<?php
session_start();


if(isset($_GET['delete'])) {
    $id =  $_GET['delete'];
    unset($_SESSION['wishlist'][$id]);
    
} else {
    $product_id = $_POST['product_id'];
    $quantity = $_POST['quantity'];
    $_SESSION['wishlist'][$product_id] = $quantity;
}
?>
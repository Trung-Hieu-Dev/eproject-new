<?php
session_start();
ob_start();

if (isset($_SESSION["customer_profile"])) {
    header("location:index.php");
    exit();
}

include './core/config.php';
include './core/connect.php';
include './model/user.php';
include './model/product/product.php';
include './model/slider.php';
include './model/banner.php';
include './model/brand.php';
include './model/category.php';

$errors = array();
$success = false;

if (isset($_POST["submit"])) {
    if (empty($_POST["email"])) {
        $errors[] = "Please enter a email address.";
    } else {
        $email = $_POST["email"];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Invalid email address.";
        }
    }

    if (empty($_POST["password"])) {
        $errors[] = "Please enter a password.";
    } else {
        $password = $_POST["password"];
        if (strlen($password) < 8) {
            $errors[] = "Password must be at least 8 characters.";
        }
        if (!preg_match("/\d/", $password)) {
            $errors[] = "Password must be contain at least one number.";
        }
        if (!preg_match("/[A-Z]/", $password)) {
            $errors[] = "Password muse be contain at least one uppercase letter.";
        }
        if (!preg_match("/[a-z]/", $password)) {
            $errors[] = "Password must be contain at least one lowercase letter.";
        }
        if (!preg_match("/\W/", $password)) {
            $errors[] = "Password must be contain at least one special character.";
        }
        if (preg_match("/\s/", $password)) {
            $errors[] = "Password should not contain any white space.";
        }
    }

    if (empty($_POST["confirm_password"])) {
        $errors[] = "Please enter a confirm password.";
    } else {
        if ($_POST['password'] !== $_POST['confirm_password']) {
            $errors[] = "Password does not match.";
        }
    }

    if (empty($errors)) {
        $data = array(
            "email" => $_POST["email"],
            "password" => $_POST["password"]
        );

        if (!check_user_exists($conn, $data["email"])) {
            register_user($conn, $data, $success);
        } else {
            $errors[] = 'Email is already exists';
        }

        if ($success) {
            header("location:login.php");
            exit();
        }
    }
}

?>

<!doctype html>
<html class="no-js" lang="en">

<head>
    <?php include './blocks/head.php' ?>
</head>

<body>
<!--header area start-->
<!--offcanvas menu area start-->
<?php include './blocks/canvas-menu.php' ?>
<!--offcanvas menu area end-->

<header>
    <?php include './blocks/header.php' ?>
</header>
<!--header area end-->

<!--breadcrumbs area start-->
<div class="breadcrumbs_area">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="breadcrumb_content">
          <h3>Register</h3>
          <ul>
            <li><a href="index.php">home</a></li>
            <li>Register</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!--breadcrumbs area end-->

<!-- customer login start -->
<div class="customer_login">
  <div class="container">
    <div class="row">
      <!--login area start-->
      <div class="container d-flex justify-content-center align-items-center">
        <div class="account_form col-lg-6 col-md-6">
          <h2>register</h2>
          <form method="post">
            <!-- Alert start -->
              <?php if (!empty($errors)) { ?>
                <div class="alert alert-danger alert-dismissible">
                  <h5><span class="lnr lnr-cross-circle"></span> Alert!</h5>
                    <?php
                    foreach ($errors as $error) {
                        echo '<li>' . $error . '</li>';
                    }
                    ?>
                </div>
              <?php } ?>
            <!-- /.alert -->
            <p>
              <label>Email <span>*</span></label>
              <input type="text" name="email">
            </p>
            <p>
              <label>Passwords <span>*</span></label>
              <input type="password" name="password">
            </p>
            <p>
              <label>Confirm Passwords <span>*</span></label>
              <input type="password" name="confirm_password">
            </p>
            <div class="login_submit">
              <a href="login.php">Already have an account ?</a>
              <button type="submit" name="submit">register</button>
            </div>
          </form>
        </div>
      </div>
      <!--login area start-->
    </div>
  </div>
</div>
<!-- customer login end -->

<!--footer area start-->
<footer class="footer_widgets footer_border">
    <?php include './blocks/footer.php' ?>
</footer>
<!--footer area end-->

<!--foot-->
<?php include './blocks/foot.php' ?>

</body>

</html>